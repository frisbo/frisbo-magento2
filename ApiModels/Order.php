<?php

namespace Frisbo\MagentoConnector\ApiModels;

class Order extends JsonConvertible
{
    public $status;
    public $reason_status;
    public $order_id;
    public $organization_id;
    public $order_reference;
    public $discount;
    public $transport_tax;
    public $channel_id;
    public $warehouse_id;
    public $notes;
    public $cash_on_delivery;
    public $ordered_date;
    public $shipped_with;
    public $shipping_tracking_number;
    public $tracking_url;
    public $is_manual;
    public $metadata = [];

    /**
     *
     * @var OrderFulfillment
     */
    public $fulfillment;

    /**
     *
     * @var OrderCustomer
     */
    public $shipping_customer;

    /**
     *
     * @var OrderAddress
     */
    public $shipping_address;

    /**
     *
     * @var OrderCustomer
     */
    public $billing_customer;

    /**
     *
     * @var OrderAddress
     */
    public $billing_address;

    /**
     *
     * @var OrderSerialNumber[]
     */
    public $serial_numbers;

    /**
     *
     * @var OrderProduct[]
     */
    public $products;

    /**
     *
     * @var OrderAttachment[]
     */
    public $attachments;

    private $_hasError = false;

    public function setHasError(bool $hasError)
    {
        $this->_hasError = $hasError;
    }

    public function getHasError(): bool
    {
        return $this->_hasError;
    }

    // these hashes are used in order for us to know wether we need to issue an update or not in Frisbo
    // the hash is the one saved in frisbo_order_statuses
    private $_hash;
    public function setHash(string $hash)
    {
        $this->_hash = $hash;
    }

    public function getHash(): ?string
    {
        return $this->_hash;
    }

    // this one is the one calculated after any changes
    private $_newHash;
    public function setNewHash(string $hash)
    {
        $this->_newHash = $hash;
    }

    public function getNewHash(): ?string
    {
        return $this->_newHash;
    }
}
