<?php

namespace Frisbo\MagentoConnector\ApiModels;

class Channel extends JsonConvertible
{
    public $id;
    public $name;
}
