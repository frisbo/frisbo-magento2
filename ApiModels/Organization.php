<?php

namespace Frisbo\MagentoConnector\ApiModels;

class Organization extends JsonConvertible
{
    public $organization_id;
    public $name;
}