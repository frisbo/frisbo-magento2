<?php

namespace Frisbo\MagentoConnector\ApiModels;

class ProductDimensions extends JsonConvertible
{
    public $height;
    public $width;
    public $length;
    public $weight;
}
