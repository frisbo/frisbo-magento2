<?php

namespace Frisbo\MagentoConnector\ApiModels\Collections;

use Frisbo\MagentoConnector\ApiModels\Product;

class ProductCollection implements \Iterator
{
    use CollectionTrait;

    public function __construct(Product ...$items)
    {
        $this->set($items);
    }

    /**
     * @return Product
     */
    public function get(int $index)
    {
        return $this->_get($index);
    }

    /**
     * @return Product
     */
    public function current(): object
    {
        return $this->_current();
    }
}
