<?php

namespace Frisbo\MagentoConnector\ApiModels\Collections;

use Frisbo\MagentoConnector\ApiModels\Channel;

class ChannelCollection implements \Iterator
{
    use CollectionTrait;

    public function __construct(Channel ...$items)
    {
        $this->set($items);
    }

    /**
     * @return Channel
     */
    public function get(int $index)
    {
        return $this->_get($index);
    }

    /**
     * @return Channel
     */
    public function current(): object
    {
        return $this->_current();
    }
}
