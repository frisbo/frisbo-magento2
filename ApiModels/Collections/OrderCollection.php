<?php

namespace Frisbo\MagentoConnector\ApiModels\Collections;

use Frisbo\MagentoConnector\ApiModels\Order;

class OrderCollection implements \Iterator
{
    use CollectionTrait;

    public function __construct(Order ...$items)
    {
        $this->set($items);
    }

    /**
     * @return Order
     */
    public function get(int $index)
    {
        return $this->_get($index);
    }

    /**
     * @return Order
     */
    public function current(): object
    {
        return $this->_current();
    }

    /**
     * @return null|Order
     */
    public function first()
    {
        return $this->_first();
    }
}
