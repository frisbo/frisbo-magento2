<?php

namespace Frisbo\MagentoConnector\ApiModels\Collections;

use Frisbo\MagentoConnector\ApiModels\Organization;

class OrganizationCollection implements \Iterator
{
    use CollectionTrait;

    public function __construct(Organization ...$items)
    {
        $this->set($items);
    }

    /**
     * @return Organization
     */
    public function get(int $index)
    {
        return $this->_get($index);
    }

    /**
     * @return Organization
     */
    public function current(): object
    {
        return $this->_current();
    }
}
