<?php

namespace Frisbo\MagentoConnector\ApiModels\Collections;

use Frisbo\MagentoConnector\ApiModels\Storage;

class StorageCollection implements \Iterator
{
    use CollectionTrait;

    public function __construct(Storage ...$items)
    {
        $this->set($items);
    }

    /**
     * @return Storage
     */
    public function get(int $index)
    {
        return $this->_get($index);
    }

    /**
     * @return Storage
     */
    public function current(): object
    {
        return $this->_current();
    }
}
