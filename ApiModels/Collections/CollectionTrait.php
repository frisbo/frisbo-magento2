<?php

namespace Frisbo\MagentoConnector\ApiModels\Collections;

trait CollectionTrait
{
    private $_position = 0;
    private $_count = 0;
    private $_items;

    public function __construct(array $items)
    {
        $this->set($items);
    }

    private function _first()
    {
        if ($this->count()) {
            return $this->_items[0];
        }
        return null;
    }

    public function items()
    {
        return $this->_items;
    }

    public function count()
    {
        return $this->_count;
    }

    private function _get(int $index)
    {
        $this->_position = $index;
        if (!$this->valid()) {
            return null;
        }
        return $this->_current();
    }

    public function rewind(): void
    {
        $this->_position = 0;
    }

    private function _current()
    {
        return $this->_items[$this->_position];
    }
    
    public function key(): int
    {
        return $this->_position;
    }

    public function next(): void
    {
        ++$this->_position;
    }

    public function valid(): bool
    {
        return isset($this->_items[$this->_position]);
    }

    public static function fromArray(array $items)
    {
        return new self(...$items);
    }

    public static function makeEmpty()
    {
        return self::fromArray([]);
    }

    public function set(array $items)
    {
        $this->_position = 0;
        $this->_items = $items;
        $this->_count = count($items);
    }
}
