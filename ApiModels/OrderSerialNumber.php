<?php

namespace Frisbo\MagentoConnector\ApiModels;

class OrderSerialNumber extends JsonConvertible
{
    public $order_id;
    public $product_id;
    public $serial_number;

    public static function create(
        string $order_id,
        string $product_id,
        string $serial_number
    ) {
        return self::fromObject(
            (object) [
                'street' => $order_id,
                'city' => $product_id,
                'county' => $serial_number,
            ]
        );
    }
}
