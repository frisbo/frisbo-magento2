<?php

namespace Frisbo\MagentoConnector\ApiModels;

class OrderAttachment extends JsonConvertible
{
    public $fileName;
    public $fileSize;
    public $type;
    public $url;

    public static function create(
        string $fileName,
        string $fileSize,
        string $type,
        string $url
    ) {
        return self::fromObject(
            (object) [
                'file_name' => $fileName,
                'file_size' => $fileSize,
                'type' => $type,
                'url' => $url
            ]
        );
    }
}