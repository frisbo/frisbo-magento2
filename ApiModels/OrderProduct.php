<?php

namespace Frisbo\MagentoConnector\ApiModels;

class OrderProduct extends JsonConvertible
{
    public $name;
    public $sku;
    public $product_id;
    public $vat;
    public $price;
    public $price_with_vat;
    public $quantity;
    public $discount;
    public $is_virtual;
    public $total;

    public static function create(
        string $name,
        string $vat,
        float $price_with_vat,
        float $quantity,
        float $discount,
        string $sku = "",
        bool $is_virtual = false
    ) {
        if ($is_virtual) {
            return self::fromObject(
                (object) [
                    'name' => $name,
                    'vat' => $vat,
                    'price_with_vat' => $price_with_vat,
                    'quantity' => $quantity,
                    'discount' => $discount,
                    'is_virtual' => $is_virtual,
                ]
            );
        }

        return self::fromObject(
            (object) [
                'name' => $name,
                'vat' => $vat,
                'price_with_vat' => $price_with_vat,
                'quantity' => $quantity,
                'discount' => $discount,
                'sku' => $sku,
            ]
        );
    }
}
