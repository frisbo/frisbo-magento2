<?php

namespace Frisbo\MagentoConnector\ApiModels;

class OrderAddress extends JsonConvertible
{
    public $street;
    public $city;
    public $county;
    public $country;
    public $zip;
    public $lockerId;

    public static function create(
        string $street,
        string $city,
        string $county,
        string $country,
        string $zip,
        string $lockerId = null
    ) {
        return self::fromObject(
            (object) [
                'street' => $street,
                'city' => $city,
                'county' => $county,
                'country' => $country,
                'zip' => $zip,
                'lockerId' => $lockerId,
            ]
        );
    }
}
