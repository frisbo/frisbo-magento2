<?php

namespace Frisbo\MagentoConnector\Helper\Traits;

trait AdHocFilters
{
    public function searchCriteriaFromArrayFilter(array $eq = [], array $in = [], array $neq = [])
    {
        $eqGroup = $this->groupsFromFilter($eq, 'eq');
        $inGroup = $this->groupsFromFilter($in, 'in');
        $neqGroup = $this->groupsFromFilter($neq, 'neq');

        return $this->_criteriaBuilder->setFilterGroups(array_merge($eqGroup,$inGroup,$neqGroup))->create();
    }

    public function groupsFromFilter(array $filters, $type = 'eq')
    {
        $typedGroups = [];
        foreach ($filters as $attributeName => $value) {
            $filter = $this->_filterBuilder->setField($attributeName)->setValue($value)->setConditionType('eq')->create();
            $typedGroups[] = $this->_filterGroupBuilder->addFilter($filter)->create();
        }
        return $typedGroups;
    }
}
