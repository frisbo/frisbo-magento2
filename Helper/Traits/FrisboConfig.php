<?php

namespace Frisbo\MagentoConnector\Helper\Traits;

use Frisbo\MagentoConnector\Helper\FrisboConst;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

trait FrisboConfig
{

    public function getSelectedOrganizationId(int $websiteId = null): int
    {
        if($websiteId) {
            $websiteApiAccount = (string) $this->scopeConfig->getValue(FrisboConst::API_GROUP_PATH . 'frisbo_api_account', 'website', $websiteId);
            if (!empty($websiteApiAccount)) {
                return (int) $websiteApiAccount;
            }
        }
        return (int) $this->scopeConfig->getValue(FrisboConst::API_GROUP_PATH . 'frisbo_api_account', ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
    }

    public function isFrisboEnabled(int $websiteId): bool
    {
        return (bool) $this->scopeConfig->getValue(FrisboConst::CHANNEL_GROUP_PATH . 'frisbo_channel_enable', 'website', $websiteId);
    }

    public function isSendInvoiceThroughEmailEnabled(int $websiteId): bool
    {
        return (bool) $this->scopeConfig->getValue(FrisboConst::CHANNEL_GROUP_PATH . 'frisbo_channel_send_invoice', 'website', $websiteId);
    }

    public function getSelectedChannelId(string $scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT, int $scopeId = null): int
    {
        return (int) $this->scopeConfig->getValue(FrisboConst::CHANNEL_GROUP_PATH . 'frisbo_channel_mapping', $scopeType, $scopeId);
    }

    public function getSelectedSourceCode(string $scopeType = 'website', int $scopeId = null): ?string
    {
        return $this->scopeConfig->getValue(FrisboConst::CHANNEL_GROUP_PATH . 'frisbo_channel_source', $scopeType, $scopeId);
    }

    public function getChannelOrderPrefix(int $websiteId)
    {
        $channelOrderPrefix = (string) $this->scopeConfig->getValue(FrisboConst::CHANNEL_GROUP_PATH . 'frisbo_channel_order_prefix', 'website', $websiteId);
        if (!empty($channelOrderPrefix)) {
            return $channelOrderPrefix;
        }
        $defaultOrderPrefix = $this->scopeConfig->getValue(FrisboConst::CHANNEL_GROUP_PATH . 'frisbo_channel_order_prefix', ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
        return (string) $defaultOrderPrefix;
    }

    public function getMsiDisabled()
    {
        return (bool) $this->scopeConfig->getValue(FrisboConst::CHANNEL_GROUP_PATH . 'frisbo_channel_msi_disabled', ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
    }

    public function getAutomaticProcessingStatuses(): array
    {
        $automaticallyProcessedStatuses = explode(",", $this->scopeConfig->getValue(FrisboConst::ADVANCED_ORDER_PATH . 'frisbo_advanced_auto_fulfill', ScopeConfigInterface::SCOPE_TYPE_DEFAULT));
        if (empty($automaticallyProcessedStatuses)) {
            $automaticallyProcessedStatuses = ['processing'];
        }
        return $automaticallyProcessedStatuses;
    }

    public function getPendingPaymentStatuses(): array
    {
        $pendingPaymentStatuses = explode(",", $this->scopeConfig->getValue(FrisboConst::ADVANCED_ORDER_PATH . 'frisbo_advanced_pending_payment', ScopeConfigInterface::SCOPE_TYPE_DEFAULT));
        if (empty($pendingPaymentStatuses)) {
            $pendingPaymentStatuses = ['pending', 'pending_payment', 'payment_review', 'pending_paypal'];
        }
        return $pendingPaymentStatuses;
    }

    public function getCancelStatuses(): array
    {
        $cancelStatuses = explode(",", $this->scopeConfig->getValue(FrisboConst::ADVANCED_ORDER_PATH . 'frisbo_advanced_pending_cancel', ScopeConfigInterface::SCOPE_TYPE_DEFAULT));
        if (empty($cancelStatuses)) {
            $cancelStatuses = ['canceled'];
        }
        return $cancelStatuses;
    }

    public function clearConfigCache()
    {
        $this->_cacheTypeList->cleanType('config');
        $this->_cacheTypeList->cleanType('compiled_config');
        $this->_cacheTypeList->cleanType('full_page');
    }

    public function isSettingSyncCompletedOrdersEnabled(int $websiteId): bool
    {
        return (bool) $this->scopeConfig->getValue(FrisboConst::CHANNEL_GROUP_PATH . 'frisbo_sync_completed_orders', 'website', $websiteId);
    }

    public function isTaxIncluded(): bool
    {
        return $this->scopeConfig->getValue('tax/calculation/price_includes_tax', ScopeInterface::SCOPE_STORE);
    }

    public function getChannelStatusForReturnedOrders(int $websiteId): ?string
    {
        $channelStatusForReturnedOrders = (string) $this->scopeConfig->getValue(FrisboConst::CHANNEL_GROUP_PATH . 'frisbo_sync_returned_orders', 'website', $websiteId);
        if (!empty($channelStatusForReturnedOrders)) {
            return $channelStatusForReturnedOrders;
        }
        $defaultOrderPrefix = $this->scopeConfig->getValue(FrisboConst::CHANNEL_GROUP_PATH . 'frisbo_sync_returned_orders', ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
        return (string) $defaultOrderPrefix;
    }

    public function getChannelStatusForBackToSenderOrders(int $websiteId): ?string
    {
        $channelStatusForBackToSenderOrders = (string) $this->scopeConfig->getValue(FrisboConst::CHANNEL_GROUP_PATH . 'frisbo_sync_backtosender_orders', 'website', $websiteId);
        if (!empty($channelStatusForBackToSenderOrders)) {
            return $channelStatusForBackToSenderOrders;
        }
        $defaultOrderPrefix = $this->scopeConfig->getValue(FrisboConst::CHANNEL_GROUP_PATH . 'frisbo_sync_backtosender_orders', ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
        return (string) $defaultOrderPrefix;
    }
}
