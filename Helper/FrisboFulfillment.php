<?php

namespace Frisbo\MagentoConnector\Helper;

use Exception;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Config\Storage\Writer;
use Magento\Framework\App\Cache\Frontend\Pool;
use Magento\Framework\App\Cache\TypeListInterface;

use Frisbo\MagentoConnector\ApiModels\Organization;
use Frisbo\MagentoConnector\ApiModels\Channel;
use Frisbo\MagentoConnector\ApiModels\Collections\OrganizationCollection;
use Frisbo\MagentoConnector\ApiModels\Collections\ChannelCollection;
use Frisbo\MagentoConnector\ApiModels\Collections\OrderCollection;
use Frisbo\MagentoConnector\ApiModels\Collections\ProductCollection;
use Frisbo\MagentoConnector\ApiModels\Filters\Filter;
use Frisbo\MagentoConnector\Exception\AccessForbiden;
use Frisbo\MagentoConnector\Logger\Logger;

use Frisbo\MagentoConnector\ApiModels\Product;
use Frisbo\MagentoConnector\ApiModels\Order;
use Frisbo\MagentoConnector\Exception\OrderException;
use Frisbo\MagentoConnector\Exception\OrderExists;
use Frisbo\MagentoConnector\Exception\ProductExists;
use Frisbo\MagentoConnector\Helper\Traits\FrisboConfig;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Catalog\Model\Product\Attribute\Repository as ProductAttributeRepository;

class FrisboFulfillment extends AbstractHelper
{

    use FrisboConfig;

    private $_frisboClient;
    private $_configWriter;
    private $_cacheTypeList;
    private $_frisboLogger;
    private $_stockHelper;
    private $_frisboProductHelper;
    private $_frisboOrderHelper;
    private $_productAttributeRepository;

    public function __construct(
        Context $context,
        Logger $logger,
        Writer $configWriter,
        TypeListInterface $cacheTypeList,
        FrisboClient $frisboClient,
        FrisboStockHelper $stockHelper,
        FrisboProductHelper $frisboProductHelper,
        FrisboOrderHelper $frisboOrderHelper,
        ProductAttributeRepository $productAttributeRepository
    )
    {
        $this->_frisboLogger = $logger;
        $this->_frisboClient = $frisboClient;
        $this->_configWriter = $configWriter;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_stockHelper = $stockHelper;
        $this->_frisboProductHelper = $frisboProductHelper;
        $this->_frisboOrderHelper = $frisboOrderHelper;
        $this->_productAttributeRepository = $productAttributeRepository;
        parent::__construct($context);
    }

    public function getClient(): FrisboClient
    {
        return $this->_frisboClient;
    }

    public function getUserOrganizations(): OrganizationCollection
    {
        $organizationsArray = [];
        try {
            $organizationsArray = json_decode($this->_frisboClient->getOrganizations());
            $this->_frisboLogger->info("FrisboFulfillment::Got ".count($organizationsArray)." organizations");
        } catch (AccessForbiden $ex) {
            $this->_frisboLogger->info("FrisboFulfillment::".$ex->getMessage());
        }
        return OrganizationCollection::fromArray(array_map([Organization::class,'fromObject'], $organizationsArray));
    }

    public function getOrganizationChannels(int $organizationId): ChannelCollection
    {
        $channelsArray = [];
        try {
            $channelsArray = json_decode($this->_frisboClient->getOrganizationChannels($organizationId));
        } catch (AccessForbiden $ex) {
            $this->_frisboLogger->info("FrisboFulfillment::" . $ex->getMessage());
        }
        return ChannelCollection::fromArray(array_map([Channel::class, 'fromObject'], $channelsArray));
    }

    public function getProductsByFilter(int $websiteId = null, Filter ...$filters): ?ProductCollection
    {
        try {
            $productsResponse = json_decode($this->_frisboClient->getProductsByFilter($this->getSelectedOrganizationId($websiteId), ...$filters));
            return $this->_frisboProductHelper->responseToFrisboProducts($productsResponse);
        } catch (AccessForbiden $ex) {
            $this->_frisboLogger->info("FrisboFulfillment::" . $ex->getMessage());
        }

        return null;
    }

    public function sendProduct(int $websiteId = null, Product $product): ?Product
    {
        try {
            $productResponse = json_decode($this->_frisboClient->sendProduct($this->getSelectedOrganizationId($websiteId), $product));
            $this->handleProductBadResponse($productResponse);
        } catch (AccessForbiden $ex) {
            $this->_frisboLogger->info("FrisboFulfillment::" . $ex->getMessage());
            return null;
        } catch (ProductExists $ex) {
            $this->_frisboLogger->info("FrisboFulfillment::" . $ex->getMessage());
            return $this->getProductsByFilter($websiteId, Filter::from('sku', $product->sku))->get(0);
        } catch (Exception $ex) {
            $this->_frisboLogger->info("FrisboFulfillment::" . $ex->getMessage());
            return null;
        }
        return Product::fromObject($productResponse);
    }

    /**
     * Sends products to Frisbo.
     * Returns null if products where sent succesfully, returns array of products failed.
     *
     * @param integer ...$magentoProductIds
     * @return Product[]
     */
    public function syncProducts($website = null, int ...$magentoProductIds): array
    {
        $websiteId = null;
        if($website) {
          $websiteId = $website->getId();
        }
        $failedProducts = [];
        $productCollection = $this->_frisboProductHelper->convertIdsToFrisboProducts(...$magentoProductIds);
        foreach ($productCollection as $product) {
            $this->_frisboLogger->info($product->toJson());
            $productCreated = $this->sendProduct($websiteId, $product);
            if ($productCreated == null) {
                $this->_frisboLogger->info("FrisboFulfillment::Failure to send: $product->sku .");
                $failed[] = $product;
                continue;
            }
            $productCreated->external_code = $product->external_code;
            $this->_frisboProductHelper->syncProduct($website, $productCreated);
        }

        return $failedProducts;
    }

    // TODO: implement this
    public function sendProductsAsync(int ...$magentoProductIds)
    {

    }

    /**
     * Syncs stocks for selected products from Frisbo.
     * Returns null if products where sent succesfully, returns array of products failed.
     *
     * @param integer ...$magentoProductIds
     */
    public function syncStocksByProductIds($website, int ...$magentoProductIds)
    {
        $magentoProducts = $this->_frisboProductHelper->getMagentoProductsByIds($magentoProductIds);
        $storeIds = $website->getStoreIds();

        $frisboProductIds = [];
        foreach($magentoProducts as $magentoProduct) {
          $resource = $magentoProduct->getResource();
          foreach($storeIds as $storeId) {
            $value = $resource->getAttributeRawValue($magentoProduct->getId(),'frisbo_product_id',$storeId);
            if($value) {
              $frisboProductIds[] = $value;
            }
          }
        }

        $filter = new Filter('product_id_in', implode(',', $frisboProductIds));
        $this->syncStocksByFilterForWebsite($website->getId(), $filter);
    }

    public function syncStocksByFilterForWebsite(int $websiteId = null, Filter ...$filters)
    {
        $sourceCode = $this->getSelectedSourceCode('website', $websiteId);
        if (!$sourceCode) {
            $this->_frisboLogger->info("FrisboFulfillment:Source is not set for $websiteId");
            return;
        }

        $assignedChannelId = $this->getSelectedChannelId('website', $websiteId);
        if ($assignedChannelId == 0) {
            $this->_frisboLogger->info("FrisboFulfillment::ChannelId is not set for $websiteId");
            return;
        }

        $assignedOrganizationId = $this->getSelectedOrganizationId($websiteId);

        $storageResponse = $this->_frisboClient->getStocksByFilter($assignedOrganizationId, $assignedChannelId, ...$filters);
        if ($storageResponse == null) {
            $this->_frisboLogger->info("FrisboFulfillment:error syncing stocks for $websiteId");
            return;
        }

        $storageCollection = $this->_stockHelper->responseToFrisboStorage(json_decode($storageResponse));
        $this->_stockHelper->saveFrisboStoragesToSource($sourceCode, $websiteId, $storageCollection);
    }

    /**
     * Syncs orders with Frisbo by magento order ids
     *
     * @param integer ...$magentoOrderIds
     * @return Order[]|null
     */
    public function syncOrders(int ...$magentoOrderIds): ?array
    {
        $failed = [];
        $orderCollection = $this->_frisboOrderHelper->convertIdsToFrisboOrders(...$magentoOrderIds);

        $alreadySyncedOrders = [];
        foreach ($orderCollection as $order) {
            // if already synced, we'll get them in bulk later
            if ($order->order_id) {
                $alreadySyncedOrders[] = $order;
                continue;
            }
            $orderCreated = $this->sendOrder($order);
            if ($orderCreated == null || $orderCreated->getHasError()) {
                $this->_frisboLogger->info("FrisboFulfillment::Failure to send: $order->order_reference.");
                $failed[] = $order;
                continue;
            }
            $this->_frisboOrderHelper->saveFrisboOrderStatus($orderCreated, $this->_frisboOrderHelper->getOrderHash($order));
        }

        $organizationOrderMap = [];
        foreach($alreadySyncedOrders as $frisboOrder) {
          $organizationOrderMap[$frisboOrder->organization_id] = $organizationOrderMap[$frisboOrder->organization_id] ?? [];
          $organizationOrderMap[$frisboOrder->organization_id][] = $frisboOrder->order_id;
        }

        foreach($organizationOrderMap as $organizationId => $frisboOrderIds) {
          $this->syncOrdersByFrisboIds($organizationId, ...$frisboOrderIds);
        }

        return $failed;
    }

    public function syncOrdersByFrisboIds(int $organizationId, int ...$frisboOrderIds)
    {
        $ordersSynced = $this->getOrders($organizationId, $frisboOrderIds);
        foreach ($ordersSynced as $orderSynced) {
            $this->_frisboOrderHelper->saveFrisboOrderStatus($orderSynced);

            if ($this->_frisboOrderHelper->isFrisboOrderInFulfillmentStatus($orderSynced)) {
                try {
                    $this->_frisboOrderHelper->syncMagentoOrderInFinalStatus($orderSynced);
                } catch(\Exception $ex) {
                    $this->_frisboLogger->info("FrisboFulfillment::Couldn't sync order status for $orderSynced->order_reference." . $ex->getMessage());
                }
            }

            try {
                $this->_frisboOrderHelper->invoiceOrder($orderSynced);
            } catch(\Exception $ex) {
                $this->_frisboLogger->info("FrisboFulfillment::Couldn't invoice $orderSynced->order_reference ." . $ex->getMessage());
            }

            try {
                $this->_frisboOrderHelper->shipOrder($orderSynced);
            } catch(\Exception $ex) {
                $this->_frisboLogger->info("FrisboFulfillment::Couldn't ship $orderSynced->order_reference ." . $ex->getMessage());
            }


        }
    }

    public function getOrders(int $organizationId, array $frisboOrderIds): OrderCollection
    {
        if (count($frisboOrderIds) == 0) {
            return OrderCollection::makeEmpty();
        }

        $ordersResponse = $this->_frisboClient->getOrdersByFilter(
            $organizationId,
            Filter::from('order_id', implode(',', $frisboOrderIds), 'in')
        );

        $frisboOrders = json_decode($ordersResponse)->data;
        if (count($frisboOrders)) {
            return OrderCollection::fromArray(
                array_map(
                    function ($order) {
                        return Order::fromObject($order);
                    },
                    $frisboOrders
                )
            );
        }

        return OrderCollection::makeEmpty();
    }

    public function sendOrder(Order $order): ?Order
    {
        try {
            $rawOrderResponse = $this->_frisboClient->sendOrder($order->organization_id, $order);
            $orderResponse = json_decode($rawOrderResponse);
            if ($orderResponse == null) {
                return null;
            }
            $this->handleOrderBadResponse($orderResponse);
            return Order::fromObject($orderResponse);
        } catch (AccessForbiden $ex) {
            $this->_frisboLogger->info("FrisboFulfillment::AccessForbidden::" . $ex->getMessage());
            return null;
        } catch (OrderExists $ex) {
            $this->_frisboLogger->info("FrisboFulfillment::OrderExists::" . $ex->getMessage());
            $order->order_id = $ex->frisboOrderId;
            return $order;
        } catch (OrderException $ex) {
            $this->_frisboLogger->info("FrisboFulfillment::OrderException::" . $ex->getMessage());
            $order->reason_status = $ex->getMessage();
            $order->setHasError(true);
            return $order;
        }
        catch (Exception $ex) {
            $this->_frisboLogger->info("FrisboFulfillment::Exception::" . $ex->getMessage());
        }

        return null;
    }

    /**
     * Ran when order status changes in Magento
     *
     * @param OrderInterface $order
     * @param string $oldStatus
     * @return void
     */
    public function magentoOrderChanged(OrderInterface $order, string $oldStatus = null)
    {
        $status = $order->getStatus();
        $frisboOrder = $this->_frisboOrderHelper->convertIdsToFrisboOrders($order->getEntityId())->first();
        if (!$frisboOrder || $frisboOrder->order_id == null) {
            if(in_array($status, $this->getCancelStatuses())){
                return;
            }
            $this->syncOrders($order->getEntityId());
            return;
        }

        $isNowInProcessingStatus = in_array($status, $this->getAutomaticProcessingStatuses());
        if ($status != $oldStatus && !$isNowInProcessingStatus && in_array($status, $this->getCancelStatuses())) {
            $this->cancelOrder($frisboOrder);
            return;
        }

        if ($oldStatus != $status && $isNowInProcessingStatus && $oldStatus && in_array($oldStatus, $this->getPendingPaymentStatuses())) {
            if ($frisboOrder->getNewHash() != $frisboOrder->getHash()) {
                // $this->_frisboLogger->info("new hash: ". $frisboOrder->getNewHash());
                // $this->_frisboLogger->info("old hash: " . $frisboOrder->getHash());
                $this->updateOrder($frisboOrder);
                sleep(2);
            } else {
                $this->_frisboLogger->info("FrisboFulfillment::Not updating $frisboOrder->order_reference since it has the same hash.");
            }
            $this->confirmPaymentForOrder($frisboOrder);
            return;
        }
    }

    public function updateOrder(Order $order)
    {
        $this->_frisboLogger->info("FrisboFulfillment:updating $order->order_reference");
        $newOrder = $this->_frisboClient->updateOrder($order->organization_id, $order);
        if ($newOrder == null) {
            $this->_frisboLogger->info("Couldn't update order $order->order_reference.");
            return;
        }

        $newFrisboOrder = Order::fromJson($newOrder);
        $this->_frisboOrderHelper->saveFrisboOrderStatus($newFrisboOrder);
    }

    public function cancelOrder(Order $order)
    {
        $newOrder = $this->_frisboClient->cancelOrder($order->organization_id, $order);
        if ($newOrder == null) {
            $this->_frisboLogger->info("Couldn't cancel order $order->order_reference.");
            return;
        }

        $this->syncOrdersByFrisboIds($order->organization_id, $order->order_id);
    }

    public function confirmPaymentForOrder(Order $order)
    {
        $this->_frisboLogger->info("FrisboFulfillment::Confirming payment for order $order->order_reference");
        $this->_frisboClient->transitionOrder($order->organization_id, $order, 'payment_confirmed');
    }

    public function setSelectedOrganizationId(int $organizationId, string $scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT, int $scopeId = 0)
    {
        $this->_configWriter->save(FrisboConst::API_GROUP_PATH . 'frisbo_api_account', $organizationId, $scopeType, $scopeId);
    }

    public function setApiSettingsValid(bool $valid)
    {
        $this->_configWriter->save(FrisboConst::API_GROUP_PATH . 'frisbo_api_valid', $valid, ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
    }

    public function setSelectedChannelId(int $channelId, string $scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT, int $scopeId = 0)
    {
        $this->_configWriter->save(FrisboConst::CHANNEL_GROUP_PATH . 'frisbo_channel_mapping', $channelId, $scopeType, $scopeId);
    }

    private function handleProductBadResponse(object $productResponse)
    {
        $hasError = (bool) ($productResponse->error ?? false);
        if ($hasError) {
            $productMessage = $productResponse->message;
            if (strpos($productMessage, 'already exists') !== false) {
                throw new ProductExists("Product already exists in Frisbo.");
            }
        }
    }

    private function handleOrderBadResponse(object $orderResponse): ?string
    {
        $hasError = (bool) ($orderResponse->error ?? false);
        if ($hasError) {
            $this->throwOrderExistsException($orderResponse->message);
            throw new OrderException($orderResponse->message);
        }

        return null;
    }

    private function throwOrderExistsException(string $message)
    {
        if (strpos($message, 'already exists') !== false) {
            throw new OrderExists($message);
        }
    }
}
