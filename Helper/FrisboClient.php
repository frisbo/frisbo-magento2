<?php

namespace Frisbo\MagentoConnector\Helper;

use Frisbo\MagentoConnector\Exception\AccessForbiden;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\BadResponseException;

use GuzzleHttp\ClientFactory;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ResponseFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;

use Exception;
use Frisbo\MagentoConnector\ApiModels\Filters\Filter;
use Frisbo\MagentoConnector\ApiModels\Order;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Frisbo\MagentoConnector\Logger\Logger;

use Frisbo\MagentoConnector\ApiModels\Product;

/**
 * FrisboClient helper
 */
class FrisboClient extends AbstractHelper
{
    private $_frisboLogger;
    private $_frisboCache;
    private $_attempts = 0;

    const API_URL = 'https://api.frisbo.ro';

    const AUTH_URL = '/v1/auth/login';
    const ORGANIZATIONS_URL = '/v1/organizations';
    const ORGANIZATIONS_CHANNEL_URL = '/v1/organizations/{organizationId}/channels';
    const ORGANIZATIONS_CHANNEL_STOCK_URL = '/v1/organizations/{organizationId}/channels/{channelId}/storage/stock';
    const ORGANIZATIONS_PRODUCT_URL = '/v1/organizations/{organizationId}/products';
    const ORGANIZATIONS_ORDERS_URL = '/v1/organizations/{organizationId}/orders';
    const ORGANIZATIONS_ORDER_URL = '/v1/organizations/{organizationId}/orders/{orderId}';
    const TRANSITION_ORDER_URL = '/v1/organizations/{organizationId}/orders/{orderId}/workflows/order_workflow/transitions/{transitionName}';

    public function __construct(Context $context, Logger $logger, FrisboCache $frisboCache)
    {
        $this->_frisboLogger = $logger;
        $this->_frisboCache = $frisboCache;
        parent::__construct($context);
    }

    private function getApiConfig(): array
    {
        $apiGroupPath = 'frisbo_api_settings/frisbo_api_group/';
        $username =  $this->scopeConfig->getValue($apiGroupPath . 'frisbo_api_username', ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
        $password =  $this->scopeConfig->getValue($apiGroupPath . 'frisbo_api_password', ScopeConfigInterface::SCOPE_TYPE_DEFAULT);
        return [$username, $password];
    }

    /**
     * Login via username and password and return json
     *
     * @param string $username
     * @param string $password
     * @return string|null
     */
    public function login(string $username = null, string $password = null):? string
    {
        if ($username == null || $password == null) {
            return null;
        }

        $credentialsBody = [
            'email' => $username,
            'password' => $password
        ];

        $client = new Client(['timeout'  => 30.0]);
        try {
            $response = $client->request(
                'POST',
                self::API_URL.self::AUTH_URL,
                [
                    RequestOptions::JSON => $credentialsBody
                ]
            );

            return $response->getBody();
        } catch (ClientException $e) {
            $this->handleClientException($e);
        } catch (\Exception $e) {
            $this->_frisboLogger->info('FrisboClient::' . $e->getMessage());
        }

        return null;
    }

    public function getAccessToken(bool $force = false):? string
    {
        if (!$force) {
            $cachedToken = $this->_frisboCache->load(FrisboCache::FRISBO_TOKEN_KEY);
            if (!empty($cachedToken)) {
                return $cachedToken;
            }
        }

        list($username, $password) = $this->getApiConfig();
        try {
            $authJsonResponse = $this->login($username, $password);
            if ($authJsonResponse === null) {
                return null;
            }
            $authResponse = json_decode($authJsonResponse);
            $this->_frisboCache->save(FrisboCache::FRISBO_TOKEN_KEY, $authResponse->access_token, $authResponse->expires_in);
            return $authResponse->access_token;
        } catch(Exception $ex) {
            $this->_frisboLogger->info('FrisboClient::'.$ex->getMessage());
        }

        $this->forgetToken();
        return null;
    }

    public function forgetToken()
    {
        $this->_frisboCache->forget(FrisboCache::FRISBO_TOKEN_KEY);
    }

    public function getOrganizations():? string
    {
        $accessToken = $this->getAccessToken();
        if (!$accessToken) {
            throw new AccessForbiden("Cannot access Frisbo Api.");
        }

        $client = new Client(['timeout'  => 30.0]);
        try {
            $response = $client->request(
                'GET',
                self::API_URL . self::ORGANIZATIONS_URL,
                [
                    RequestOptions::HEADERS => ['Authorization' => 'Bearer '.$accessToken]
                ]
            );

            return $response->getBody();
        } catch (ClientException $e) {
            $this->handleClientException($e);
        } catch (\Exception $e) {
            $this->_frisboLogger->info('FrisboClient::' . $e->getMessage());
        }

        return null;
    }

    public function getOrganizationChannels(int $organizationId): ?string
    {
        $accessToken = $this->getAccessToken();
        if (!$accessToken) {
            throw new AccessForbiden("Cannot access Frisbo Api.");
        }

        $client = new Client(['timeout'  => 30.0]);
        try {
            $response = $client->request(
                'GET',
                self::API_URL . str_replace('{organizationId}', $organizationId, self::ORGANIZATIONS_CHANNEL_URL),
                [
                    RequestOptions::HEADERS => ['Authorization' => 'Bearer ' . $accessToken]
                ]
            );

            return $response->getBody();
        } catch (ClientException $e) {
            $this->handleClientException($e);
        } catch (\Exception $e) {
            $this->_frisboLogger->info('FrisboClient::' . $e->getMessage());
        }

        return null;
    }

    public function sendProduct($organizationId, Product $product): ?string
    {
        $accessToken = $this->getAccessToken();
        if (!$accessToken) {
            throw new AccessForbiden("Cannot access Frisbo Api.");
        }

        $client = new Client(['timeout'  => 30.0]);
        try {
            $response = $client->request(
                'POST',
                self::API_URL . str_replace('{organizationId}', $organizationId, self::ORGANIZATIONS_PRODUCT_URL),
                [
                    RequestOptions::HEADERS => ['Authorization' => 'Bearer ' . $accessToken],
                    RequestOptions::JSON => (array) $product
                ]
            );

            return $response->getBody();
        } catch (BadResponseException $e) {
            $this->_frisboLogger->info('FrisboClient::'.$e->getMessage());
            return $this->handleClientException($e);
        } catch (Exception $e) {
            $this->_frisboLogger->info('FrisboClient::' . $e->getMessage());
        }

        return null;
    }

    public function sendOrder($organizationId, Order $order): ?string
    {
        $accessToken = $this->getAccessToken();
        if (!$accessToken) {
            throw new AccessForbiden("Cannot access Frisbo Api.");
        }

        $client = new Client(['timeout'  => 30.0]);
        try {
            $response = $client->request(
                'POST',
                self::API_URL . str_replace('{organizationId}', $organizationId, self::ORGANIZATIONS_ORDERS_URL),
                [
                    RequestOptions::HEADERS => ['Authorization' => 'Bearer ' . $accessToken],
                    RequestOptions::JSON => (array) $order
                ]
            );

            return $response->getBody();
        } catch (BadResponseException $e) {
            $this->_frisboLogger->info('FrisboClient::' . $e->getMessage());
            return $this->handleClientException($e);
        } catch (Exception $e) {
            $this->_frisboLogger->info('FrisboClient::' . $e->getMessage());
        }

        return null;
    }

    public function updateOrder($organizationId, Order $order): ?string
    {
        $accessToken = $this->getAccessToken();
        if (!$accessToken) {
            throw new AccessForbiden("Cannot access Frisbo Api.");
        }

        $client = new Client(['timeout'  => 30.0]);
        try {
            $response = $client->request(
                'PUT',
                self::API_URL . str_replace(['{organizationId}','{orderId}'], [$organizationId, $order->order_id], self::ORGANIZATIONS_ORDER_URL),
                [
                    RequestOptions::HEADERS => ['Authorization' => 'Bearer ' . $accessToken],
                    RequestOptions::JSON => (array) $order
                ]
            );

            return $response->getBody();
        } catch (BadResponseException $e) {
            $this->_frisboLogger->info('FrisboClient::' . $e->getMessage());
            return $this->handleClientException($e, true);
        } catch (Exception $e) {
            $this->_frisboLogger->info('FrisboClient::' . $e->getMessage());
        }

        return null;
    }

    public function cancelOrder($organizationId, Order $order): ?string
    {
        $accessToken = $this->getAccessToken();
        if (!$accessToken) {
            throw new AccessForbiden("Cannot access Frisbo Api.");
        }

        $client = new Client(['timeout'  => 30.0]);
        try {
            $response = $client->request(
                'DELETE',
                self::API_URL . str_replace(['{organizationId}', '{orderId}'], [$organizationId, $order->order_id], self::ORGANIZATIONS_ORDER_URL),
                [
                    RequestOptions::HEADERS => ['Authorization' => 'Bearer ' . $accessToken],
                    RequestOptions::JSON => (array) $order
                ]
            );

            return $response->getBody();
        } catch (BadResponseException $e) {
            $this->_frisboLogger->info('FrisboClient::' . $e->getMessage());
            return $this->handleClientException($e);
        } catch (Exception $e) {
            $this->_frisboLogger->info('FrisboClient::' . $e->getMessage());
        }

        return null;
    }

    public function transitionOrder($organizationId, Order $order, string $transitionName): ?string
    {
        $accessToken = $this->getAccessToken();
        if (!$accessToken) {
            throw new AccessForbiden("Cannot access Frisbo Api.");
        }

        $this->_frisboLogger->info(self::API_URL . str_replace(['{organizationId}', '{orderId}', '{transitionName}'], [$organizationId, $order->order_id, $transitionName], self::TRANSITION_ORDER_URL));

        $client = new Client(['timeout'  => 30.0]);
        try {
            $response = $client->request(
                'POST',
                self::API_URL . str_replace(['{organizationId}', '{orderId}', '{transitionName}'], [$organizationId, $order->order_id, $transitionName], self::TRANSITION_ORDER_URL),
                [
                    RequestOptions::HEADERS => ['Authorization' => 'Bearer ' . $accessToken],
                ]
            );

            return $response->getBody();
        } catch (BadResponseException $e) {
            $this->_frisboLogger->info('FrisboClient::' . $e->getMessage());
            return $this->handleClientException($e);
        } catch (Exception $e) {
            $this->_frisboLogger->info('FrisboClient::' . $e->getMessage());
        }

        return null;
    }

    public function getProductsByFilter($organizationId, Filter ...$filters): ?string
    {
        $accessToken = $this->getAccessToken();
        if (!$accessToken) {
            throw new AccessForbiden("Cannot access Frisbo Api.");
        }

        $client = new Client(['timeout'  => 30.0]);
        try {
            $response = $client->request(
                'GET',
                self::API_URL . str_replace('{organizationId}', $organizationId, self::ORGANIZATIONS_PRODUCT_URL) . '?'. Filter::combineFilters(...$filters),
                [
                    RequestOptions::HEADERS => ['Authorization' => 'Bearer ' . $accessToken]
                ]
            );

            return $response->getBody();
        } catch (BadResponseException $e) {
            $this->_frisboLogger->info('FrisboClient::' . $e->getMessage());
            return $this->handleClientException($e);
        } catch (Exception $e) {
            $this->_frisboLogger->info('FrisboClient::' . $e->getMessage());
        }

        return null;
    }

    public function getOrdersByFilter($organizationId, Filter ...$filters): ?string
    {
        $accessToken = $this->getAccessToken();
        if (!$accessToken) {
            throw new AccessForbiden("Cannot access Frisbo Api.");
        }

        $client = new Client(['timeout'  => 30.0]);
        try {
            $response = $client->request(
                'GET',
                self::API_URL . str_replace('{organizationId}', $organizationId, self::ORGANIZATIONS_ORDERS_URL) . '?' . Filter::combineFilters(...$filters),
                [
                    RequestOptions::HEADERS => ['Authorization' => 'Bearer ' . $accessToken]
                ]
            );

            return $response->getBody();
        } catch (BadResponseException $e) {
            $this->_frisboLogger->info('FrisboClient::' . $e->getMessage());
            return $this->handleClientException($e);
        } catch (Exception $e) {
            $this->_frisboLogger->info('FrisboClient::' . $e->getMessage());
        }

        return null;
    }

    public function getStocksByFilter($organizationId, $channelId, Filter ...$filters): ?string
    {
        $accessToken = $this->getAccessToken();
        if (!$accessToken) {
            throw new AccessForbiden("Cannot access Frisbo Api.");
        }

        $client = new Client(['timeout'  => 30.0]);
        try {
            $response = $client->request(
                'GET',
                self::API_URL . str_replace(['{organizationId}', '{channelId}'], [ $organizationId, $channelId], self::ORGANIZATIONS_CHANNEL_STOCK_URL) . '?' . Filter::combineFilters(...$filters),
                [
                    RequestOptions::HEADERS => ['Authorization' => 'Bearer ' . $accessToken]
                ]
            );

            return $response->getBody();
        } catch (BadResponseException $e) {
            $this->_frisboLogger->info('FrisboClient::' . $e->getMessage());
            return $this->handleClientException($e);
        } catch (Exception $e) {
            $this->_frisboLogger->info('FrisboClient::' . $e->getMessage());
        }

        return null;
    }

    private function handleClientException(BadResponseException $e, bool $nullOnServerError = false):? string
    {
        if ($e->hasResponse()) {
            if ($e->getResponse()->getStatusCode() == '403') {
                throw new AccessForbiden("Invalid username or password");
            }
            if ($nullOnServerError && $e->getResponse()->getStatusCode() == '500') {
                return null;
            }
            return $e->getResponse()->getBody();
        }

        return null;
    }
}
