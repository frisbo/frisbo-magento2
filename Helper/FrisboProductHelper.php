<?php

namespace Frisbo\MagentoConnector\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Frisbo\MagentoConnector\Logger\Logger;
use Magento\Catalog\Model\ProductRepository;
use Magento\Catalog\Model\Product\Action as ProductAction;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Catalog\Api\Data\ProductInterface;

use Frisbo\MagentoConnector\ApiModels\Product;
use Frisbo\MagentoConnector\ApiModels\Collections\ProductCollection;
use Frisbo\MagentoConnector\Helper\Traits\AdHocFilters;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as MagentoProductCollectionFactory;

class FrisboProductHelper extends AbstractHelper
{
    use AdHocFilters;

    private $_frisboLogger;
    private $_criteriaBuilder;
    private $_productRepository;
    private $_productAction;
    private $_filterBuilder;
    private $_filterGroupBuilder;
    private $_magentoProductCollection;

    const DEFAULT_ATTRIBUTES_MAP = [
        'frisbo_product_id' => 'frisbo_product_id',
        'product_advanced_ean' => 'frisbo_product_ean',
        'product_advanced_height' => 'frisbo_product_height',
        'product_advanced_width' => 'frisbo_product_width',
        'product_advanced_length' => 'frisbo_product_length',
        'product_advanced_has_serial_number' => 'frisbo_product_has_serial_number'
    ];

    public function __construct(
        Context $context,
        Logger $frisboLogger,
        FilterGroupBuilder $filterGroupBuilder,
        FilterBuilder $filterBuilder,
        SearchCriteriaBuilder $criteriaBuilder,
        ProductRepository $productRepository,
        ProductAction $productAction,
        MagentoProductCollectionFactory $magentoProductCollection
    ) {
        $this->_frisboLogger = $frisboLogger;
        $this->_criteriaBuilder = $criteriaBuilder;
        $this->_productRepository = $productRepository;
        $this->_productAction = $productAction;
        $this->_filterBuilder = $filterBuilder;
        $this->_filterGroupBuilder = $filterGroupBuilder;
        $this->_magentoProductCollection = $magentoProductCollection;
        parent::__construct($context);
    }

    /**
     * Gets mapped custom attributes
     *
     * @return array
     */
    public function getCustomAttributesCode(): array
    {
        $advancedAttributesPath = [
            'product_id' => 'frisbo_product_id',
            'ean' => 'product_advanced_ean',
            'height' => 'product_advanced_height',
            'width' => 'product_advanced_width',
            'length' => 'product_advanced_length',
            'has_serial_number' => 'product_advanced_has_serial_number'
        ];

        $advancedProductPath = 'frisbo_advanced_settings/frisbo_advanced_product';

        foreach ($advancedAttributesPath as $key => $value) {
            $configValue = $this->scopeConfig->getValue($advancedProductPath . '/' . $value);
            if (empty($configValue)) {
                $configValue = self::DEFAULT_ATTRIBUTES_MAP[$value];
            }
            $advancedAttributesPath[$key] = $configValue;
        }

        return $advancedAttributesPath;
    }

    /**
     * Gets Magento products by ids
     *
     * @param array $magentoProductIds
     *
     * @return \Magento\Catalog\Api\Data\ProductInterface[]
     */
    public function getMagentoProductsByIds(array $magentoProductIds = []): array
    {
        $filterEntity = $this->_filterBuilder->setField('entity_id')->setValue($magentoProductIds)->setConditionType('in')->create();
        $filterEntityGroup = $this->_filterGroupBuilder->addFilter($filterEntity)->create();

        $filterType = $this->_filterBuilder->setField('type_id')->setValue('simple')->setConditionType('eq')->create();
        $filterTypeGroup = $this->_filterGroupBuilder->addFilter($filterType)->create();
        $searchCriteria = $this->_criteriaBuilder->setFilterGroups([$filterEntityGroup, $filterTypeGroup])->create();

        return $this->_productRepository->getList($searchCriteria)->getItems();
    }

    /**
     * Gets Magento products by sku
     *
     * @param array $magentoProductIds
     *
     * @return \Magento\Catalog\Api\Data\ProductInterface
     */
    public function getMagentoProductsBySku(string $sku): array
    {
        $filterEntity = $this->_filterBuilder->setField('sku')->setValue($sku)->setConditionType('in')->create();
        $filterEntityGroup = $this->_filterGroupBuilder->addFilter($filterEntity)->create();

        $searchCriteria = $this->_criteriaBuilder->setFilterGroups([$filterEntityGroup])->create();

        return $this->_productRepository->getList($searchCriteria)->getItems();
    }

    public function getUnsyncedMagentoProductIds(int $storeId, int $limit = 10): array
    {
        $productCollection = $this->_magentoProductCollection->create();
        $collection = $productCollection->addAttributeToSelect('*')
            ->addStoreFilter($storeId)
            ->addAttributeToFilter('type_id', \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE)
            ->addAttributeToFilter(
                array(
                    array('attribute' => 'frisbo_product_id', 'null' => true),
                    array('attribute' => 'frisbo_product_id', 'eq' => ''),
                    array('attribute' => 'frisbo_product_id', 'eq' => 'NULL')
                ),
                '',
                'left'
            )
            ->addOrder('created_at')
            ->setPageSize($limit);

        $productsNotSynced = $collection->load()->getItems();

        $magentoProductIds = [];
        foreach ($productsNotSynced as $unsyncedProduct) {
            $magentoProductIds[] = $unsyncedProduct->getId();
        }

        return $magentoProductIds;
    }

    public function convertIdsToFrisboProducts(int ...$magentoProductIds): ProductCollection
    {
        $magentoProducts = $this->getMagentoProductsByIds($magentoProductIds);
        return $this->convertMagentoProductsToFrisboProducts($magentoProducts);
    }

    public function convertMagentoProductsToFrisboProducts(array $magentoProducts): ProductCollection
    {
        $customAttributesCodes = $this->getCustomAttributesCode();

        return ProductCollection::fromArray(
            array_map(
                function ($magentoProduct) use ($customAttributesCodes) {
                    return $this->convertToFrisboProduct($magentoProduct, $customAttributesCodes);
                },
                $magentoProducts
            )
        );
    }

    public function responseToFrisboProducts(object $frisboPaginatedProductResponse): ProductCollection
    {
        $productsData = $frisboPaginatedProductResponse->data ?? [];
        if (count($productsData) == 0) {
            return ProductCollection::makeEmpty();
        }
        return ProductCollection::fromArray(
            array_map(
                function ($product) {
                    return Product::fromObject($product);
                },
                $productsData
            )
        );
    }

    /**
     *
     * @param ProductInterface $product
     * @param array $customAttributesCodes
     * @return Product
     */
    public function convertToFrisboProduct(ProductInterface $product, array $customAttributesCodes = []): Product
    {
        $product_id = $this->getProductCustomAttribute($product, $customAttributesCodes['product_id'], '0');
        $ean = $this->getProductCustomAttribute($product, $customAttributesCodes['ean'], 'undefined');
        $height = $this->getProductCustomAttribute($product, $customAttributesCodes['height'], '0.1');
        $width = $this->getProductCustomAttribute($product, $customAttributesCodes['width'], '0.1');
        $length = $this->getProductCustomAttribute($product, $customAttributesCodes['length'], '0.1');
        $has_serial_number = (bool) $this->getProductCustomAttribute($product, $customAttributesCodes['has_serial_number'], false);

        return Product::fromObject(
            (object) [
                'product_id' => $product_id,
                'sku' => $product->getSku(),
                'name' => $product->getName(),
                'external_code' => $product->getId(),
                'type' => 'product',
                'upc' => $ean,
                'ean' => $ean,
                'vat' => '19',
                'dimensions' => [
                    'height' => $height,
                    'width' => $width,
                    'length' => $length,
                    'weight' => $product->getWeight()
                ],
                'has_serial_number' => $has_serial_number
            ]
        );
    }

    /**
     * Syncs Product to Magento db.
     * Assumes magento product id is stored in external_code
     *
     * @param Product $product
     * @return void
     */
    public function syncProduct($website = null, Product $product = null)
    {
        if (!$product) {
            return;
        }
        $magentoProduct = $this->_productRepository->getById($product->external_code, true);
        $attributeCodes = $this->getCustomAttributesCode(); 

        $storeIds = $website->getStoreIds();
        $websiteStores = $magentoProduct->getStoreIds();
        $storesToUpdate = array_intersect($storeIds, $websiteStores);

        $overwriteAttributes = (bool) $this->scopeConfig->getValue('frisbo_advanced_settings/frisbo_advanced_product/frisbo_advanced_overwrite_attributes');

        foreach ($storesToUpdate as $storeToUpdate) {
            foreach ($attributeCodes as $key => $attributeCode) {
                switch($key) {
                    case 'product_id':
                        $this->_frisboLogger->info("FrisboProductConverter::Setting $attributeCode of $product->external_code to " . $product->{$key} . " for store $storeToUpdate");
                        $this->_productAction->updateAttributes([$magentoProduct->getId()], [$attributeCode => $product->{$key}], $storeToUpdate);
                        break;
                    case 'height':
                    case 'width':
                    case 'length':
                        if (!$overwriteAttributes) {
                            break;
                        }
                        $this->_frisboLogger->info("FrisboProductConverter::Setting $attributeCode of $product->external_code to " . $product->dimensions->{$key} . " for store $storeToUpdate");
                        $this->_productAction->updateAttributes([$magentoProduct->getId()], [$attributeCode => $product->dimensions->{$key}], $storeToUpdate);
                        break;
                    default:
                        if (!$overwriteAttributes) {
                            break;
                        }
                        $this->_frisboLogger->info("FrisboProductConverter::Setting $attributeCode of $product->external_code to " . $product->{$key} . " for store $storeToUpdate");
                        $this->_productAction->updateAttributes([$magentoProduct->getId()], [$attributeCode => $product->{$key}], $storeToUpdate);
                        break;
                }
            }
        }
    }

    /**
     *
     * @param ProductInterface $product
     * @param string $attributeCode
     * @param string $default
     * @return void
     */
    private function getProductCustomAttribute(ProductInterface $product, string $attributeCode, string $default = null)
    {
        $customAttribute = $product->getCustomAttribute($attributeCode);
        return $customAttribute ? $customAttribute->getValue() : $default;
    }
}
