<?php

namespace Frisbo\MagentoConnector\Helper;

use Frisbo\MagentoConnector\ApiModels\Collections\StorageCollection;
use Frisbo\MagentoConnector\ApiModels\Storage;
use Frisbo\MagentoConnector\Helper\Traits\FrisboConfig;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\InventoryApi\Api\GetStockSourceLinksInterface;
use Magento\InventoryApi\Model\GetSourceCodesBySkusInterface;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\InventorySales\Model\GetAssignedSalesChannelsForStock;
use Magento\InventorySalesApi\Api\Data\SalesChannelInterface;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Magento\InventoryApi\Api\SourceItemsSaveInterface;
use Magento\InventoryApi\Api\Data\SourceItemInterfaceFactory;
use Magento\Catalog\Model\ResourceModel\Product\Action as UpdateProductAttributesAction;
use Magento\Catalog\Model\Product;
class FrisboStockHelper extends AbstractHelper
{

    use FrisboConfig;

    private $_sourceBySku;
    private $_stockBySourceCode;
    private $_filterGroupBuilder;
    private $_filterBuilder;
    private $_criteriaBuilder;
    private $_salesChannelByStock;
    private $_websiteRepository;
    private $_sourceItemsSaveInterface;
    private $_sourceItemFactory;
    private $_updateProductAttributesAction;
    private $_productCollection;

    public function __construct(
        Context $context,
        GetSourceCodesBySkusInterface $sourceBySku,
        GetStockSourceLinksInterface $stockBySourceCode,
        FilterGroupBuilder $filterGroupBuilder,
        FilterBuilder $filterBuilder,
        SearchCriteriaBuilder $criteriaBuilder,
        GetAssignedSalesChannelsForStock $salesChannelsByStock,
        WebsiteRepositoryInterface $websiteRepositoryInterface,
        SourceItemsSaveInterface $sourceItemsSaveInterface,
        SourceItemInterfaceFactory $sourceItemFactory,
        UpdateProductAttributesAction $updateProductAttributesAction,
        Product $productCollection
    )
    {
        $this->_sourceBySku = $sourceBySku;
        $this->_stockBySourceCode = $stockBySourceCode;
        $this->_filterBuilder = $filterBuilder;
        $this->_filterGroupBuilder = $filterGroupBuilder;
        $this->_criteriaBuilder = $criteriaBuilder;
        $this->_salesChannelByStock = $salesChannelsByStock;
        $this->_websiteRepository = $websiteRepositoryInterface;
        $this->_sourceItemsSaveInterface = $sourceItemsSaveInterface;
        $this->_sourceItemFactory = $sourceItemFactory;
        $this->_updateProductAttributesAction = $updateProductAttributesAction;
        $this->_productCollection = $productCollection;
        parent::__construct($context);
    }

    public function getSourceCodesBySkus(array $skus): array
    {
        return $this->_sourceBySku->execute($skus);
    }

    public function getStockIdsBySourceCodes(array $sourceCodes): array
    {
        $sourceCodeFilter = $this->_filterBuilder
            ->setField('source_code')
            ->setConditionType('in')
            ->setValue($sourceCodes)
            ->create();

        $searchCriteria = $this->_criteriaBuilder->addFilters([$sourceCodeFilter])->create();
        $stocks = $this->_stockBySourceCode->execute($searchCriteria)->getItems();

        return array_map(
            function ($stock) {
                return $stock->getStockId();
            },
            $stocks
        );
    }

    public function getWebsiteCodesByStockIds(array $stockIds): array
    {
        $websiteCodes = [];
        foreach ($stockIds as $stockId) {
            $stockSalesChannels = $this->_salesChannelByStock->execute($stockId);
            foreach ($stockSalesChannels as $stockSalesChannel) {
                if ($stockSalesChannel->getType() == SalesChannelInterface::TYPE_WEBSITE) {
                    $websiteCodes[] = $stockSalesChannel->getCode();
                }
            }
        }

        return $websiteCodes;
    }

    public function getEnabledMagentoWebsites(): array
    {
        return array_filter($this->_websiteRepository->getList(), function($website) {
          return $this->isFrisboEnabled($website->getId());
        });
    }

    public function getMagentoWebsiteIds(): array
    {
        $websites = $this->_websiteRepository->getList();
        return array_map(
            function ($website) {
                return $website->getId();
            },
            $websites
        );
    }

    public function getWebsitesByWebsiteCodes(array $websiteCodes): array
    {
      return array_map(
        function ($websiteCode) {
            return $this->_websiteRepository->get($websiteCode);
        },
        $websiteCodes
    );
    }

    public function getWebsiteIdsByWebsiteCodes(array $websiteCodes): array
    {
        return array_map(
            function ($websiteCode) {
                return $this->_websiteRepository->get($websiteCode)->getId();
            },
            $websiteCodes
        );
    }

    public function responseToFrisboStorage(array $frisboStoragesResponse): StorageCollection
    {
        return StorageCollection::fromArray(
            array_map(
                function ($storage) {
                    return Storage::fromObject($storage);
                },
                $frisboStoragesResponse
            )
        );
    }

    public function saveFrisboStoragesToSource(string $sourceCode, int $websiteId, StorageCollection $storages)
    {

        $sourceItems = [];
        foreach ($storages as $storage) {
            $sourceItem = $this->_sourceItemFactory->create();
            $sourceItem->setSourceCode($sourceCode);
            $sourceItem->setSku($storage->product->sku);

            $sourceItem->setQuantity($storage->scriptic_stock);
            $inStock = $storage->available_stock > 0 ? true: false;
            if ($inStock == true) {
                $sourceItem->setStatus(1);
            } else {
                $sourceItem->setStatus(0);
            }
            $sourceItems[] = $sourceItem;
        }

        if (count($sourceItems)) {
            $this->_sourceItemsSaveInterface->execute($sourceItems);
        }

    }

    public function getReservedQuantityForSku(string $sku, int $websiteId): int
    {
        $entityId = $this->_productCollection->getIdBySku($sku);
        $attrRawVal = $this->_updateProductAttributesAction->getAttributeRawValue($entityId, 'frisbo_product_reservations', $websiteId);
        return (int) $attrRawVal;
    }

    public function setReservedQuantityForSku(string $sku, int $websiteId, int $reservedQuantity)
    {
        $entityId = $this->_productCollection->getIdBySku($sku);
        $this->_updateProductAttributesAction->updateAttributes([$entityId], ['frisbo_product_reservations' => $reservedQuantity], $websiteId);
    }
}
