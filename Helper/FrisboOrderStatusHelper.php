<?php

namespace Frisbo\MagentoConnector\Helper;

use Frisbo\MagentoConnector\Logger\Logger;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Frisbo\MagentoConnector\Model\FrisboOrderStatusRepository;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Framework\Api\SearchCriteria;
use Magento\Framework\Api\SortOrder;

class FrisboOrderStatusHelper extends AbstractHelper
{
    const FINAL_STATUSES = [
        "delivered",
        "returned",
        "canceled",
        "Delivered",
        "Back to sender",
        "Canceled",
        "Returned"
    ];

    private $_searchCriteria;
    private $_filterGroupBuilder;
    private $_filterBuilder;
    private $_frisboOrderStatusRepository;
    private $_frisboLogger;

    public function __construct(
        Context $context,
        SearchCriteria $searchCriteria,
        FilterGroupBuilder $filterGroupBuilder,
        FilterBuilder $filterBuilder,
        FrisboOrderStatusRepository $frisboOrderStatusRepository,
        Logger $frisboLogger
    ) {
        $this->_searchCriteria = $searchCriteria;
        $this->_filterGroupBuilder = $filterGroupBuilder;
        $this->_filterBuilder = $filterBuilder;
        $this->_frisboOrderStatusRepository = $frisboOrderStatusRepository;
        $this->_frisboLogger = $frisboLogger;
        parent::__construct($context);
    }

    public function getNextMagentoOrderIdsToSync(int $limit = 20): array
    {
        $sortOrder = (new SortOrder())->setField('updated_at')->setDirection('asc');

        $filterStatus = $this->_filterBuilder->setField('status')->setValue(self::FINAL_STATUSES)->setConditionType('nin')->create();
        $filterStatusGroup = $this->_filterGroupBuilder->addFilter($filterStatus)->create();

        $searchCriteria = $this->_searchCriteria
            ->setSortOrders([$sortOrder])
            ->setFilterGroups([$filterStatusGroup])
            ->setPageSize($limit);

        /** @var \Frisbo\MagentoConnector\Api\FrisboOrderStatusSearchResultInterface $frisboOrderStatusesSearchResult */
        $frisboOrderStatusesSearchResult = $this->_frisboOrderStatusRepository->getList($searchCriteria);
        
        $magentoOrderIds = [];
        $frisboOrderStatuses = $frisboOrderStatusesSearchResult->getItems();
        foreach ($frisboOrderStatuses as $frisboOrderStatus) {
            $magentoOrderIds[] = $frisboOrderStatus->getMagentoOrderId();
        }

        return $magentoOrderIds;
    }

    public function getCompletedMagentoOrderIdsToSync(int $limit = 20): array
    {
        $sortOrder = (new SortOrder())->setField('updated_at')->setDirection('asc');

        $filterStatus = $this->_filterBuilder->setField('status')->setValue(self::FINAL_STATUSES)->setConditionType('in')->create();
        $filterStatusGroup = $this->_filterGroupBuilder->addFilter($filterStatus)->create();
        
        $searchCriteria = $this->_searchCriteria
            ->setSortOrders([$sortOrder])
            ->setFilterGroups([$filterStatusGroup])
            ->setPageSize($limit);

        /** @var \Frisbo\MagentoConnector\Api\FrisboOrderStatusSearchResultInterface $frisboOrderStatusesSearchResult */
        $frisboOrderStatusesSearchResult = $this->_frisboOrderStatusRepository->getList($searchCriteria);

        $magentoOrderIds = [];
        $frisboOrderStatuses = $frisboOrderStatusesSearchResult->getItems();
        foreach ($frisboOrderStatuses as $frisboOrderStatus) {
            $magentoOrderIds[] = $frisboOrderStatus->getMagentoOrderId();
        }

        return $magentoOrderIds;
    }

}