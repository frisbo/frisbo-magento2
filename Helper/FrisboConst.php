<?php

namespace Frisbo\MagentoConnector\Helper;

class FrisboConst
{
    const API_GROUP_PATH = 'frisbo_api_settings/frisbo_api_group/';
    const CHANNEL_GROUP_PATH = 'frisbo_channel_settings/frisbo_channel_group/';
    const ADVANCED_ORDER_PATH = 'frisbo_advanced_settings/frisbo_advanced_order/';
}