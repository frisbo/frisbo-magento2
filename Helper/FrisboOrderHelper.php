<?php

namespace Frisbo\MagentoConnector\Helper;

use DateTime;
use DateTimeZone;
use Exception;
use Frisbo\MagentoConnector\ApiModels\Order;
use Frisbo\MagentoConnector\ApiModels\OrderAddress;
use Frisbo\MagentoConnector\ApiModels\OrderCustomer;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Frisbo\MagentoConnector\Logger\Logger;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Directory\Api\CountryInformationAcquirerInterface;

use Frisbo\MagentoConnector\Helper\Traits\AdHocFilters;
use Magento\Sales\Api\Data\OrderInterface;
use Frisbo\MagentoConnector\ApiModels\Collections\OrderCollection;
use Frisbo\MagentoConnector\ApiModels\OrderProduct;
use Frisbo\MagentoConnector\Helper\Traits\FrisboConfig;
use Frisbo\MagentoConnector\Model\FrisboOrderStatus;
use Magento\Store\Model\StoreManagerInterface;

use Magento\Sales\Model\Order as OrderModel;
use Frisbo\MagentoConnector\Model\FrisboOrderStatusRepository;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Shipping\Model\ShipmentNotifier;
use Magento\Sales\Model\Order\ShipmentFactory;
use Magento\Sales\Model\Order\ShipmentRepository;

use Magento\Sales\Model\Service\InvoiceService;
use Magento\Framework\DB\TransactionFactory;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;

use Magento\Sales\Api\ShipOrderInterface;
use Magento\Sales\Api\Data\ShipmentItemInterfaceFactory;
use Magento\Sales\Api\Data\ShipmentTrackInterfaceFactory;
use Magento\Sales\Api\Data\ShipmentCreationArgumentsExtensionFactory;
use Magento\Sales\Model\Order\Shipment\CreationArguments;
use Magento\Sales\Model\Order\Shipment\ItemCreation;
use Magento\Bundle\Model\Product\Price;
use Magento\Sales\Model\Order\Item;

class FrisboOrderHelper extends AbstractHelper
{
    use AdHocFilters, FrisboConfig;

    private $_frisboLogger;
    private $_criteriaBuilder;
    private $_filterBuilder;
    private $_filterGroupBuilder;
    private $_storeManager;
    private $_countryInfoAcquirer;
    private $_orderRepository;
    private $_orderModel;
    private $_frisboOrderStatusRepository;
    private $_shipOrder;
    private $_shipmentItemFactory;
    private $_shipmentTrackFactory;
    private $_invoiceService;
    private $_transactionFactory;
    private $_invoiceSender;
    private $_shipmentCreationArgumentsExtensionFactory;

    public function __construct(
        Context $context,
        Logger $frisboLogger,
        FilterGroupBuilder $filterGroupBuilder,
        FilterBuilder $filterBuilder,
        SearchCriteriaBuilder $criteriaBuilder,
        StoreManagerInterface $storeManager,
        CountryInformationAcquirerInterface $countryInfoAcquirer,
        OrderRepositoryInterface $orderRepository,
        OrderModel $orderModel,
        FrisboOrderStatusRepository $frisboOrderStatusRepository,
        ShipOrderInterface $shipOrder,
        ShipmentItemInterfaceFactory $shipmentItemFactory,
        ShipmentTrackInterfaceFactory $shipmentTrackFactory,
        InvoiceService $invoiceService,
        TransactionFactory $transactionFactory,
        InvoiceSender $invoiceSender,
        ShipmentCreationArgumentsExtensionFactory $shipmentCreationArgumentsExtensionFactory
    ) {
        $this->_frisboLogger = $frisboLogger;
        $this->_criteriaBuilder = $criteriaBuilder;
        $this->_filterBuilder = $filterBuilder;
        $this->_filterGroupBuilder = $filterGroupBuilder;
        $this->_storeManager = $storeManager;
        $this->_countryInfoAcquirer = $countryInfoAcquirer;
        $this->_orderRepository = $orderRepository;
        $this->_orderModel = $orderModel;
        $this->_shipOrder = $shipOrder;
        $this->_shipmentItemFactory = $shipmentItemFactory;
        $this->_shipmentTrackFactory = $shipmentTrackFactory;
        $this->_invoiceService = $invoiceService;
        $this->_transactionFactory = $transactionFactory;
        $this->_invoiceSender = $invoiceSender;
        $this->_frisboOrderStatusRepository = $frisboOrderStatusRepository;
        $this->_shipmentCreationArgumentsExtensionFactory = $shipmentCreationArgumentsExtensionFactory;
        parent::__construct($context);
    }

    public function convertIdsToFrisboOrders(int ...$magentoOrderIds): OrderCollection
    {
        $magentoOrders = $this->getMagentoOrdersByIds(...$magentoOrderIds);
        return $this->convertMagentoOrdersToFrisboOrders($magentoOrders);
    }

    public function getMagentoOrdersByIds(int ...$magentoOrderIds): array
    {
        $filterEntity = $this->_filterBuilder->setField('entity_id')->setValue($magentoOrderIds)->setConditionType('in')->create();
        $filterEntityGroup = $this->_filterGroupBuilder->addFilter($filterEntity)->create();

        $searchCriteria = $this->_criteriaBuilder->setFilterGroups([$filterEntityGroup])->create();

        return $this->_orderRepository->getList($searchCriteria)->getItems();
    }

    public function getMagentoIncrementId(string $magentoIncrementId)
    {
        $prefixIncrement = explode('_', $magentoIncrementId, 2);
        if (isset($prefixIncrement[1])) {
            $magentoIncrementId = $prefixIncrement[1];
        }
        return $magentoIncrementId;
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface[] $magentoOrders
     * @return OrderCollection
     */
    public function convertMagentoOrdersToFrisboOrders(array $magentoOrders): OrderCollection
    {
        return OrderCollection::fromArray(
            array_map(
                function ($magentoOrder) {
                    return $this->convertMagentoOrderToFrisboOrder($magentoOrder);
                },
                $magentoOrders
            )
        );
    }

    /**
     * Converts a magento order into a Frisbo Order
     * If order is already synced, it will also set the order_id field
     *
     * @param OrderInterface $magentoOrder
     * @return Order
     */
    public function convertMagentoOrderToFrisboOrder(OrderInterface $magentoOrder): Order
    {
        $orderPrefix = $this->getChannelOrderPrefixByStoreId($magentoOrder->getStoreId());

        $paymentMethod = $magentoOrder->getPayment()->getMethod();

        $billing = $magentoOrder->getBillingAddress();
        $shipping = $magentoOrder->getShippingAddress() ?? $billing;

        $orderDate = new DateTime($magentoOrder->getCreatedAt());
        $orderDate->setTimezone(new DateTimeZone('UTC'));

        $frisboOrderId = null;

        $existingHash = null;
        $frisboOrderStatus = $this->getFrisboOrderStatusByMagentoId($magentoOrder->getEntityId());
        if ($frisboOrderStatus) {
            $frisboOrderId = $frisboOrderStatus->getOrderId();
            $existingHash = $frisboOrderStatus->getOrderHash();
        }

        $orderProducts = $this->convertMagentoProductLinesToFrisboProductLines($magentoOrder->getItems());

        $orderProductTotal = array_sum(array_map(function($orderProduct){
            return $orderProduct->total;
        }, $orderProducts));

        // calculate discount
        $shippingTax = floatval($magentoOrder->getShippingInclTax());
        $baseGrandTotal = floatval($magentoOrder->getBaseGrandTotal());
        $discount = ($orderProductTotal + $shippingTax) - $baseGrandTotal;
        $discount = $discount > -0.001 && $discount < 0.001 ? 0 : $discount;
        $status = $magentoOrder->getStatus();

        $frisboOrder = Order::fromObject(
            (object) [
                'order_id' => $frisboOrderId,
                'organization_id' => $this->getSelectedOrganizationId($this->getWebsiteIdFromStoreId($magentoOrder->getStoreId())),
                'order_reference' => $orderPrefix.'_'. $magentoOrder->getIncrementId(),
                'channel_id' => $this->getSelectedChannelId('website', $this->getWebsiteIdFromStoreId($magentoOrder->getStoreId())),
                'warehouse_id' => -1,
                'cash_on_delivery' => $paymentMethod == 'cashondelivery' ? true : false,
                'transport_tax' => $magentoOrder->getShippingInclTax(),
                'notes' => 'PaymentMethod: ' . $paymentMethod . ' ' . $magentoOrder->getCustomerNote(),
                'discount' => $discount,
                'is_manual' => !empty($status) ? !$this->isStatusToBeAutomaticallyProcessed($status) : true,
                'ordered_date' => $orderDate->format('Y-m-d H:i:s'),
                'shipping_customer' => OrderCustomer::create(
                    ($shipping->getCompany() == null ? $shipping->getFirstname(): $shipping->getCompany()),
                    ($shipping->getCompany() == null ? $shipping->getLastname() : $shipping->getFirstname() . ' ' . $shipping->getLastname()),
                    $shipping->getTelephone(),
                    $this->_coalesce($shipping->getEmail(), $billing->getEmail(), $magentoOrder->getIncrementId() . "@magento.com"),
                    ($shipping->getCompany() == null ? null: $shipping->getVatId()),
                    ($shipping->getCompany() == null ? null: '.')
                ),
                'shipping_address' => OrderAddress::create(
                    trim(implode(' ', $shipping->getStreet())),
                    $shipping->getCity(),
                    $shipping->getRegion() ? $shipping->getRegion() : '-',
                    $this->_getCountryName($shipping->getCountryId()),
                    $shipping->getPostcode(),
                ),
                'billing_customer' => OrderCustomer::create(
                    ($billing->getCompany() == null ? $billing->getFirstname(): $billing->getCompany()),
                    ($billing->getCompany() == null ? $billing->getLastname() : '.'),
                    $billing->getTelephone(),
                    $this->_coalesce($billing->getEmail(), $shipping->getEmail(), $magentoOrder->getIncrementId() . "@magento.com"),
                    ($billing->getCompany() == null ? null: $billing->getVatId()),
                    ($billing->getCompany() == null ? null: '.')
                ),
                'billing_address' => OrderAddress::create(
                    trim(implode(' ', $billing->getStreet())),
                    $billing->getCity(),
                    $billing->getRegion() ? $billing->getRegion() : '-',
                    $this->_getCountryName($billing->getCountryId()),
                    $billing->getPostcode(),
                ),
                'products' => $orderProducts
            ]
        );

        if ($this->getBaseCurrencyCode($magentoOrder->getStoreId())) {
            $frisboOrder->metadata = [
                "payment_currency" => $this->getBaseCurrencyCode($magentoOrder->getStoreId()),
            ];
        }

        if ($existingHash) {
            $frisboOrder->setHash($existingHash);
        }
        $frisboOrder->setNewHash($this->getOrderHash($frisboOrder));
        return $frisboOrder;
    }

    public function isStatusToBeAutomaticallyProcessed(string $status): bool
    {
        return in_array($status, $this->getAutomaticProcessingStatuses());
    }

    /**
     *
     * @param \Magento\Sales\Api\Data\OrderItemInterface[] $magentoProductLines
     * @return \Frisbo\MagentoConnector\ApiModels\OrderProduct[]
     */
    public function convertMagentoProductLinesToFrisboProductLines(array $magentoProductLines): array
    {
        $orderProducts = [];
        foreach ($magentoProductLines as $productLine) {
            $productType = $productLine->getProductType();
            // we are only interested in simple and virtual products so we can skip configurable products
            if($productType && ($productType == 'configurable' || $productType == 'bundle')) {
                continue;
            }
            $quantity = $productLine->getQtyOrdered();
            $isVirtual = (bool) $productLine->getIsVirtual();
            $name = $productLine->getName();
            $tax = $productLine->getTaxPercent();
            $priceWithVat = $this->getPriceWithVat($productLine);
            $discount = $productLine->getBaseDiscountAmount();
            $total = ($quantity * $priceWithVat) - $discount;
            if($this->isTaxIncluded() ) {
                $total = $productLine->getRowTotalInclTax() - $productLine->getBaseDiscountAmount();
            }

            $parent = $productLine->getParentItem();
            $isPriceFixed = $parent ? $parent->getProduct()->getPriceType() == Price::PRICE_TYPE_FIXED : false;
            if ($parent && $isPriceFixed && !is_null($productLine->getProduct())) {
                $product = $productLine->getProduct();
                $tax = $parent->getTaxPercent();
                $name = $name . ' ('. $parent->getName() . ')';
                $priceWithVat = $productLine->getTaxPercent() == 0 ? $product->getPrice(): $product->getPrice() + $product->getTaxAmount()/$quantity;
                $total = ($quantity * $priceWithVat) - $discount;
            }

            $orderProduct = OrderProduct::create(
                $name,
                $tax ?? '0',
                $priceWithVat,
                $productLine->getQtyOrdered(),
                $discount,
                $productLine->getSku(),
                $isVirtual
            );
            $orderProduct->total = $total;
            $orderProducts[] = $orderProduct;
        }

        foreach ($magentoProductLines as $productLine) {
            if (is_null($productLine->getProduct())) {
                continue;
            }
            $productType = $productLine->getProductType();
            $isPriceFixed = $productLine->getProduct()->getPriceType() == Price::PRICE_TYPE_FIXED;

            if ($productType == 'bundle' && $isPriceFixed) {
                $orderProducts[] = $this->handleFixedPriceBundleProduct($productLine);
            }
        }

        return $orderProducts;
    }

    public function getPriceWithVat($product)
    {
        if($product->getTaxPercent() == 0 && $product->getPriceInclTax() === null) {
            return $product->getPrice();
        }

        if($this->isTaxIncluded()) {
            return $product->getPriceInclTax();
        }

        return $product->getPrice() + $product->getTaxAmount() / $product->getQtyOrdered();
    }

    public function saveFrisboOrderStatus(Order $order, string $orderHash = null)
    {
        $magentoIncrementId = $this->getMagentoIncrementId($order->order_reference);
        $orderModel = $this->_orderModel->loadByIncrementId($magentoIncrementId);

        if ($orderModel == null) {
            $this->_frisboLogger->info("FrisboOrderConverter::order not found in magento: ".$order->order_reference);
            return;
        }

        $frisboOrderStatus = $this->getFrisboOrderStatusByMagentoId($orderModel->getEntityId());
        if ($frisboOrderStatus == null) {
            $frisboOrderStatus = $this->_frisboOrderStatusRepository->create();
            if (!$orderHash) {
                $frisboOrderStatus->setOrderHash($this->getOrderHash($order));
            } else {
                $frisboOrderStatus->setOrderHash($orderHash);
            }
        }

        $frisboOrderStatus->setMagentoOrderId($orderModel->getEntityId());

        $orderIdSet = false;

        if ($order->order_id && \is_numeric($order->order_id)) {
            $frisboOrderStatus->setOrderId((int)$order->order_id);
            $orderIdSet = true;
        }

        if ($order->status) {
            if ($order->fulfillment && $order->fulfillment->status) {
                $frisboOrderStatus->setStatus($order->fulfillment->status);
            } else {
                $frisboOrderStatus->setStatus($order->status);
            }
        }

        if ($order->reason_status) {
            $frisboOrderStatus->setReasonStatus($order->reason_status);
        }

        if ($order->shipping_tracking_number) {
            $frisboOrderStatus->setShippingTrackingNumber($order->shipping_tracking_number);
        }

        if ($order->tracking_url) {
            $frisboOrderStatus->setAwbUrl($order->tracking_url);
        }

        if ($order->serial_numbers && is_array($order->serial_numbers)) {
            $serialNumbers = implode(",", array_column($order->serial_numbers, 'serial_number'));
            $frisboOrderStatus->setSerialNumber($serialNumbers);
        }

        if ($orderIdSet) {
            $frisboOrderStatus->setUpdatedAt(date_create()->format('Y-m-d H:i:s'));
            $this->_frisboOrderStatusRepository->save($frisboOrderStatus);
            $this->_frisboLogger->info("FrisboOrderConverter::Saved frisbo order status for ".$order->order_id);
        }

    }

    public function shipOrder(Order $frisboOrder)
    {
        if (!$this->isPastWaitingForCourier($frisboOrder->fulfillment->status)) {
            $this->_frisboLogger->info("FrisboOrderHelper::Too soon to be shipped $frisboOrder->order_reference");
            return;
        }

        $magentoIncrementId = $this->getMagentoIncrementId($frisboOrder->order_reference);
        $order = $this->_orderModel->loadByIncrementId($magentoIncrementId);

        // to check order can ship or not
        if (!$order->canShip()) {
            $this->_frisboLogger->info("FrisboOrderHelper::Cannot ship $magentoIncrementId");
            return;
        }

        $trackData = $this->_shipmentTrackFactory->create();
        $trackData->setCarrierCode('custom');
        $trackData->setTitle($frisboOrder->shipped_with);
        $trackData->setTrackNumber($frisboOrder->shipping_tracking_number);

        $shipmentItems = [];
        foreach ($order->getAllItems() as $orderItem) {
            if (!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                continue;
            }
            $shipmentItem = new ItemCreation();
            $shipmentItem->setOrderItemId($orderItem->getItemId());
            $shipmentItem->setQty($orderItem->getQtyToShip());
            $shipmentItems[] = $shipmentItem;
        }

        $websiteId = $order->getStore()->getWebsiteId();
        $sourceCode = $this->getSelectedSourceCode('website', $websiteId);
        $shipmentCreationArgumentsExtension = $this->_shipmentCreationArgumentsExtensionFactory->create();
        $shipmentCreationArgumentsExtension->setSourceCode($sourceCode);

        $creationArguments = new CreationArguments();
        $creationArguments->setExtensionAttributes($shipmentCreationArgumentsExtension);

        $shipmentId = $this->_shipOrder->execute($order->getEntityId(), $shipmentItems, true, true, null, [$trackData], [], $creationArguments);
        $this->_frisboLogger->info("FrisboOrderHelper::Created shipment $shipmentId for $frisboOrder->order_reference");
    }

    public function syncMagentoOrderInFinalStatus(Order $frisboOrder)
    {
        $magentoIncrementId = $this->getMagentoIncrementId($frisboOrder->order_reference);
        $order = $this->_orderModel->loadByIncrementId($magentoIncrementId);

        switch($frisboOrder->fulfillment->status) {
            case 'Returned':
                $status = $this->getStatusForReturnedOrders($order->getId());
                $orderStateToSync = 'complete';
                $orderStatusToSync = strtolower($status);
                break;
            case 'Back to sender':
                $status = $this->getStatusForBackToSenderOrders($order->getId());
                $orderStateToSync = 'complete';
                $orderStatusToSync = strtolower($status);
                break;
            case 'Canceled':
                $orderStateToSync = 'canceled';
                $orderStatusToSync = 'canceled';
                break;
            case 'Delivered':
                $orderStateToSync = 'complete';
                $orderStatusToSync = 'complete';
                break;
            default:
                $orderStateToSync = 'closed';
                $orderStatusToSync = 'closed';
                break;
        }

        if ($order->getState() == $orderStateToSync && $order->getStatus() == $orderStatusToSync) {
            $this->_frisboLogger->info("FrisboOrderHelper::Order status `" . $order->getStatus() .  "` and order state `" . $order->getState() ."` for this order $frisboOrder->order_reference is already synced.");
            return;
        }

        $this->syncMagentoOrderStatus($order, $frisboOrder->order_reference, $orderStateToSync, $orderStatusToSync);
    }

    private function syncMagentoOrderStatus(OrderModel $order, string $frisboOrderReference,  string $orderStateToSync, string $orderStatusToSync)
    {
        $this->_frisboLogger->info("FrisboOrderHelper::Changing status/state from `" . $order->getStatus() . "/" . $order->getState() . "` in `$orderStatusToSync/$orderStateToSync` for order $frisboOrderReference.");
        $order->setState($orderStateToSync)->setStatus($orderStatusToSync);
        $order->addStatusToHistory($order->getStatus(), "Order changed in status '$orderStatusToSync' successfully.");
        $order->save();
    }

    public function isFrisboOrderInFulfillmentStatus(Order $frisboOrder): bool
    {
        $frisboStatusesToLookFor = [
            'Returned', 
            'Back to sender', 
            'Canceled', 
            'Delivered'
        ];

        return in_array($frisboOrder->fulfillment->status, $frisboStatusesToLookFor);
    }

    private function isPastWaitingForCourier(string $status)
    {
        if( strpos($status,"External awb") !== false ||
            strpos($status,"In transit") !== false ||
            strpos($status,"Delivered") !== false ||
            strpos($status,"Returned") !== false ||
            strpos($status,"Back to sender") !== false ||
            strpos($status,"Waiting for courier") !== false ){
            return true;
        }
        return false;
    }

    private function isPastReadyForPicking(string $status)
    {
        if(strpos($status,"Ready for picking") !== false ||
            strpos($status,"External awb") !== false ||
            strpos($status,"In transit") !== false ||
            strpos($status,"Delivered") !== false ||
            strpos($status,"Returned") !== false ||
            strpos($status,"Back to sender") !== false ||
            strpos($status,"Waiting for courier") !== false ){
            return true;
        }
        return false;
    }

    public function invoiceOrder(Order $frisboOrder)
    {
        if (!$this->isPastReadyForPicking($frisboOrder->fulfillment->status)) {
            $this->_frisboLogger->info("FrisboOrderHelper::Couldn't invoice order since it didn't pass Ready for picking status in Frisbo. $frisboOrder->order_reference");
            return;
        }

        $magentoIncrementId = $this->getMagentoIncrementId($frisboOrder->order_reference);
        $order = $this->_orderModel->loadByIncrementId($magentoIncrementId);
        if (!$order->getId()) {
            throw new \Magento\Framework\Exception\LocalizedException(__('The order no longer exists.'));
        }
        if (!$order->canInvoice()) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('The order does not allow an invoice to be created.')
            );
        }

        $invoice = $this->_invoiceService->prepareInvoice($order);
        if (!$invoice) {
            throw new \Magento\Framework\Exception\LocalizedException(__('We can\'t save the invoice right now.'));
        }
        if (!$invoice->getTotalQty()) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('You can\'t create an invoice without products.')
            );
        }
        $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_OFFLINE);
        $invoice->register();
        $invoice->getOrder()->setCustomerNoteNotify(false);
        $invoice->getOrder()->setIsInProcess(true);
        $order->addStatusHistoryComment('Automatically INVOICED by Frisbo', false);
        $transactionSave = $this->_transactionFactory->create()->addObject($invoice)->addObject($invoice->getOrder());
        $transactionSave->save();

        if ($this->isSendInvoiceByEmailTrueForStoreId($order->getStoreId())) {
            try {
                $this->invoiceSender->send($invoice);
            } catch (\Exception $e) {
                $this->_frisboLogger->info("FrisboOrderHelper::Couldn't send invoice by email.");
            }
        }
    }

    public function getOrderHash(Order $order): string
    {
        $valueToHash = "";
        $valueToHash .= $order->order_reference;
        $valueToHash .= ($order->discount ? number_format(round($order->discount, 2), 2, '.', ''): 'd');
        $valueToHash .= ($order->transport_tax ? number_format(round($order->transport_tax, 2), 2, '.', '') : 't');
        $valueToHash .= $this->getAddressHash($order->billing_address);
        $valueToHash .= $this->getAddressHash($order->shipping_address);
        $valueToHash .= $this->getCustomerHash($order->shipping_customer);
        $valueToHash .= $this->getCustomerHash($order->billing_customer);
        $valueToHash .= $this->getProductsHash($order->products);
        // WARNING: do not change this order of hashing if you don't know what you're doing
        return md5($valueToHash);
    }

    /**
     * Get OrderProducts hash
     *
     * @param OrderProduct[] $orderProducts
     * @return string
     */
    private function getProductsHash(array $orderProducts): string
    {
        $valueToHash = "";

        $hashes = array_map(
            function ($orderProduct) {
                return ($orderProduct->name ?? "n")
                    . number_format(round($orderProduct->quantity, 2), 2, '.', '')
                    . number_format(round($orderProduct->price_with_vat, 2), 2, '.', '')
                    . number_format(round($orderProduct->discount ?? 0, 2), 2, '.', '')
                    . number_format(round($orderProduct->vat, 2), 2, '.', '')
                    . ($orderProduct->is_virtual ? '1': '0');
            },
            $orderProducts
        );
        sort($hashes);
        return md5($valueToHash.implode('', $hashes));
    }

    /**
     * Gets hash from address
     *
     * @param OrderAddress $orderAddress
     * @return string
     */
    private function getAddressHash($orderAddress): string
    {
        $valueToHash = $orderAddress->street . $orderAddress->city . $orderAddress->county . $orderAddress->country . $orderAddress->zip;
        return md5($valueToHash);
    }

    /**
     * Gets hash from customer
     *
     * @param OrderCustomer $orderAddress
     * @return string
     */
    private function getCustomerHash($orderCustomer): string
    {
        $valueToHash = $orderCustomer->first_name . $orderCustomer->last_name . $orderCustomer->phone . $orderCustomer->trade_register_registration_number . $orderCustomer->vat_registration_number . $orderCustomer->email;
        return md5($valueToHash);
    }

    public function getFrisboOrderStatusByMagentoId(int $magentoId): ?FrisboOrderStatus
    {
        try {
            return $this->_frisboOrderStatusRepository->getByMagentoOrderId($magentoId);
        } catch (NoSuchEntityException $ex) {
            $this->_frisboLogger->info($ex->getMessage());
        }

        return null;
    }


    public function getChannelOrderPrefixByStoreId(int $storeId): string
    {
        $websiteId = $this->getWebsiteIdFromStoreId($storeId);
        $channelOrderPrefix = $this->getChannelOrderPrefix($websiteId);
        return empty($channelOrderPrefix) ? 'MGNT' : $channelOrderPrefix;
    }

    public function isSendInvoiceByEmailTrueForStoreId(int $storeId): bool
    {
        $websiteId = $this->getWebsiteIdFromStoreId($storeId);
        $isSendInvoiceByEmailTrue = $this->isSendInvoiceThroughEmailEnabled($websiteId);
        return $isSendInvoiceByEmailTrue;
    }

    public function getWebsiteIdFromStoreId(int $storeId): int
    {
        return $this->_storeManager->getStore($storeId)->getWebsiteId();
    }

    /**
     * @param string $countryCode
     * @return string
     */
    private function _getCountryName(string $countryId)
    {
        try {
            $country = $this->_countryInfoAcquirer->getCountryInfo($countryId);
            return $country->getFullNameEnglish();
        } catch (Exception $ex) {
            $this->_frisboLogger->info("FrisboOrderConverter::No country name found for country id $countryId");
        }

        return '-';
    }

    private function _coalesce(...$args)
    {
        foreach ($args as $arg) {
            //Check if it's a function and sets its return value as the one to check
            if (is_array($arg) && isset($arg[0]) && strpos($arg[0], '()') !== false) {
                $callable = substr(array_shift($arg), 0, -2);
                $arg = call_user_func_array($callable, $arg);
            }
            //If it's non null, return it
            if (isset($arg)) {
                return $arg;
            }
        }
        //If there's no non-null value, return null
        return null;
    }

    public function getBaseCurrencyCode($storeId)
    {
        return $this->_storeManager->getStore($storeId)->getBaseCurrencyCode();
    }

    public function allowSyncForThisCompletedOrder(int $orderId): bool
    {
        $storeId = $this->_orderModel->load($orderId)->getStoreId();

        $websiteId = $this->getWebsiteIdFromStoreId($storeId);

        if (!$this->isSettingSyncCompletedOrdersEnabled($websiteId)) {
            return false;
        }

        if (!$this->getCompletedOrdersIdsFromPast31Days($orderId)) {
            return false;
        }
        
        return true;
    }

    public function getCompletedOrdersIdsFromPast31Days(int $orderId): bool
    {
        $lastMonth = strtotime('-31 days', strtotime(date('Y-m-d H:i:s')));
        $orderFrom = date('Y-m-d H:i:s', $lastMonth);

        $order = $this->_orderModel->load($orderId);

        if ($order->getCreatedAt() >= $orderFrom) {
            return true;
        }

        return false;
    }

    private function handleFixedPriceBundleProduct(Item $bundle): OrderProduct
    {
        $quantity = $bundle->getQtyOrdered();
        $bundleTotal = $quantity * ($bundle->getPrice() + $bundle->getTaxAmount()/$quantity) - $bundle->getBaseDiscountAmount();
        if($this->isTaxIncluded()) {
            $bundleTotal = $bundle->getRowTotalInclTax() - $bundle->getBaseDiscountAmount();
        }

        $productsSum = 0;
        foreach ($bundle->getChildrenItems() as $product) {
            $productsSum += $product->getQtyOrdered() * $product->getProduct()->getPrice();
        }

        $bundleName = "Discount " . $bundle->getName() . ' x ' . (int)$bundle->getQtyOrdered();
        $discount = $productsSum - $bundleTotal;

        $bundleObj = OrderProduct::create(
            $bundleName,
            '19',
            -$discount,
            1,
            0,
            '',
            true
        ); 

        $bundleObj->total = -$discount;

        return $bundleObj;
    }

    public function getStatusForReturnedOrders(int $orderId): string
    {
        $storeId = $this->_orderModel->load($orderId)->getStoreId();

        $websiteId = $this->getWebsiteIdFromStoreId($storeId);

        $status = $this->getChannelStatusForReturnedOrders($websiteId);
        return !empty($status) ? $status : 'complete';
    }

    public function getStatusForBackToSenderOrders(int $orderId): string
    {
        $storeId = $this->_orderModel->load($orderId)->getStoreId();

        $websiteId = $this->getWebsiteIdFromStoreId($storeId);

        $status = $this->getChannelStatusForBackToSenderOrders($websiteId);
        return !empty($status) ? $status : 'complete';
    }
}
