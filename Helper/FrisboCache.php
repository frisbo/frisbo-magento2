<?php

namespace Frisbo\MagentoConnector\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

class FrisboCache extends AbstractHelper
{
    const CACHE_TAG = 'FRISBO_CACHE';
    const CACHE_ID = 'frisbo_cache';
    const CACHE_LIFETIME = 86400;

    /**
     * Cache key used for Frisbo auth token
     */
    const FRISBO_TOKEN_KEY = 'frisbo_cache_token';

    protected $cache;
    protected $cacheState;

    /**
     * Cache constructor.
     * @param Helper\Context $context
     * @param \Magento\Framework\App\Cache $cache
     * @param \Magento\Framework\App\Cache\State $cacheState
     */
    public function __construct(
        Context $context,
        \Magento\Framework\App\Cache $cache,
        \Magento\Framework\App\Cache\State $cacheState
    ) {
        $this->cache = $cache;
        $this->cacheState = $cacheState;
        parent::__construct($context);
    }

    /**
     * @param $cacheId
     * @return bool|string
     */
    public function load(string $key)
    {
        if ($this->cacheState->isEnabled(self::CACHE_ID)) {
            return $this->cache->load($key);
        }

        return false;
    }

    /**
     * @param string $key
     * @param string $value
     * @param int $cacheLifetime
     * @return bool
     */
    public function save(string $key, string $value, $cacheLifetime = self::CACHE_LIFETIME): bool
    {
        if ($this->cacheState->isEnabled(self::CACHE_ID)) {
            $this->cache->save($value, $key, array(self::CACHE_TAG), $cacheLifetime);
            return true;
        }
        return false;
    }

    /**
     * Forget cache key
     *
     * @param string $key
     * @return boolean
     */
    public function forget(string $key): bool
    {
        if ($this->cacheState->isEnabled(self::CACHE_ID)) {
            $this->cache->save('', $key, array(self::CACHE_TAG), self::CACHE_LIFETIME);
        }

        return false;
    }
}
