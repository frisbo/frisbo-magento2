<?php

namespace Frisbo\MagentoConnector\Block\System\Config;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class ValidateCredentials extends Field
{
    /**
     * @var string
     */
    protected $_template = 'Frisbo_MagentoConnector::validateCredentials.phtml';

    /**
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * Remove scope label
     *
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $element->unsScope()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    /**
     * Return element html
     *
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }

    /**
     * Return ajax url for validate button
     *
     * @return string
     */
    public function getAjaxUrl()
    {
        return $this->getUrl('magentoconnector/configuration/validate');
    }

    /**
     * Generate validate button html
     *
     * @return string
     */
    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Button'
        )->setData(
            [
                'id' => 'frisbo_api_validate_button',
                'label' => __('Test Credentials'),
                'title' => __('Test Credentials'),
            ]
        );

        return $button->toHtml();
    }
}
