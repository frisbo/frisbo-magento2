<?php

namespace Frisbo\MagentoConnector\Api\Data;

/**
 * Interface FrisboOrderInterface
 */
interface FrisboOrderStatusInterface
{
    const ID = 'id';
    const MAGENTO_ORDER_ID = 'magento_order_id';
    const ORDER_ID = 'order_id';
    const STATUS = 'status';
    const REASON_STATUS = 'reason_status';
    const SHIPPING_TRACKING_NUMBER = 'shipping_tracking_number';
    const SERIAL_NUMBER = 'serial_number';
    const AWB_URL = 'awb_url';
    const ORDER_HASH = 'order_hash';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     *
     * @return self
     */
    public function setMagentoOrderId(int $id);

    /**
     * @return int|null
     */
    public function getMagentoOrderId();

    /**
     * @param int $id
     *
     * @return self
     */
    public function setOrderId(int $id);

    /**
     * @return int|null
     */
    public function getOrderId();

    /**
     * @param string $name
     *
     * @return self
     */
    public function setStatus(string $name);

    /**
     * @return null|string
     */
    public function getStatus();

    /**
     * @param string $name
     *
     * @return self
     */
    public function setReasonStatus(string $name);

    /**
     * @return null|string
     */
    public function getReasonStatus();

    /**
     * @param string $trackingNumber
     *
     * @return self
     */
    public function setShippingTrackingNumber(string $trackingNumber);

    /**
     * @return null|string
     */
    public function getShippingTrackingNumber();

    /**
     * @param string $serialNumber
     *
     * @return self
     */
    public function setSerialNumber(string $serialNumber);

    /**
     * @return null|string
     */
    public function getSerialNumber();

    /**
     * @param string $url
     *
     * @return self
     */
    public function setAwbUrl(string $url);

    /**
     * @return null|string
     */
    public function getAwbUrl();

    /**
     * @return null|string
     */
    public function getOrderHash();

    /**
     * * @param string $hash
     * @return self
     */
    public function setOrderHash(string $hash);

    /**
     * @param string $date
     *
     * @return self
     */
    public function setCreatedAt(string $date);

    /**
     * @return null|string
     */
    public function getCreatedAt();

    /**
     * @param string $date
     *
     * @return self
     */
    public function setUpdatedAt(string $date);

    /**
     * @return null|string
     */
    public function getUpdatedAt();

}
