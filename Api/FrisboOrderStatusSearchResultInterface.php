<?php

namespace Frisbo\MagentoConnector\Api;

use Magento\Framework\Api\SearchResultsInterface;
use Frisbo\MagentoConnector\Api\Data\FrisboOrderStatusInterface;

interface FrisboOrderStatusSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return FrisboOrderStatusInterface[]
     */
    public function getItems();

    /**
     * @param FrisboOrderStatusInterface[] $items
     * @return void
     */
    public function setItems(array $items);
}
