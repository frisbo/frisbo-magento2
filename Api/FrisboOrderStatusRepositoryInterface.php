<?php

namespace Frisbo\MagentoConnector\Api;

use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\NoSuchEntityException;
use Frisbo\MagentoConnector\Model\FrisboOrderStatus;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface AwbRepositoryInterface
 */
interface FrisboOrderStatusRepositoryInterface
{
    /**
     * @param FrisboOrderStatus $frisboOrderStatus
     *
     * @throws \Exception
     * @throws AlreadyExistsException
     */
    public function save(FrisboOrderStatus $frisboOrderStatus);

    /**
     * @param int $id
     * @return FrisboOrderStatus
     * @throws NoSuchEntityException
     */
    public function getById(int $id): FrisboOrderStatus;

    /**
     * @param int $magentoOrderId
     * @return FrisboOrderStatus
     * @throws NoSuchEntityException
     */
    public function getByMagentoOrderId(int $magentoOrderId): FrisboOrderStatus;

    /**
     * @return FrisboOrderStatus
     * @throws NoSuchEntityException
     */
    public function create(): FrisboOrderStatus;


    /**
     * Load FrisboOrderStatuses collection by given search criteria
     *
     * @param SearchCriteriaInterface $criteria
     * @return Frisbo\MagentoConnector\Api\FrisboOrderStatusSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $criteria);
}
