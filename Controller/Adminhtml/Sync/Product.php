<?php

namespace Frisbo\MagentoConnector\Controller\Adminhtml\Sync;

use Frisbo\MagentoConnector\Helper\FrisboFulfillment;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Ui\Component\MassAction\Filter;
use Frisbo\MagentoConnector\Helper\FrisboStockHelper;

/**
 * Class Product
 */
class Product extends Action
{

    private $_filter;
    private $_managerInterface;
    private $_collectionFactory;
    private $_frisboFulfillment;
    private $_frisboStockHelper;

    protected $redirectUrl = 'catalog/product/';

    /**
     * @param Context $context
     * @param FrisboFulfillment $frisboClient
     */
    public function __construct(
        Context $context,
        Filter $filter,
        ManagerInterface $managerInterface,
        CollectionFactory $collectionFactory,
        FrisboFulfillment $frisboFulfillment,
        FrisboStockHelper $frisboStockHelper
    ) {
        $this->_collectionFactory = $collectionFactory;
        $this->_managerInterface = $managerInterface;
        $this->_frisboFulfillment = $frisboFulfillment;
        $this->_frisboStockHelper = $frisboStockHelper;
        $this->_filter = $filter;
        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $collection = $this->_filter->getCollection($this->_collectionFactory->create());
            $this->massAction($collection);
        } catch (\Exception $e) {
            $this->_managerInterface->addErrorMessage($e->getMessage());
        }

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath($this->redirectUrl);
    }

    public function massAction(Collection $collection)
    {
        $productIds = array_column(array_values($collection->toArray(['entity_id'])), 'entity_id');

        $magentoWebsites = $this->_frisboStockHelper->getEnabledMagentoWebsites();
        foreach ($magentoWebsites as $website) {
          // TODO: implement this regarding advanced setting
          $failedProducts = $this->_frisboFulfillment->syncProducts($website, ...$productIds);
          if (count($failedProducts) == 0) {
              $this->_managerInterface->addSuccessMessage("Products sent to Frisbo.");
          } else {
              foreach ($failedProducts as $failedProduct) {
                  $this->_managerInterface->addErrorMessage("Failure to send: $failedProduct->sku");
              }
          }
        }
    }
}
