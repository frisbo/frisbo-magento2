<?php

namespace Frisbo\MagentoConnector\Controller\Adminhtml\Sync;

use Frisbo\MagentoConnector\Helper\FrisboFulfillment;
use Frisbo\MagentoConnector\Helper\FrisboOrderStatusHelper;
use Frisbo\MagentoConnector\Helper\FrisboOrderHelper;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;
use Magento\Sales\Model\ResourceModel\Order\Collection;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Ui\Component\MassAction\Filter;
use Frisbo\MagentoConnector\Logger\Logger;

/**
 * Class Order
 */
class Order extends Action
{

    private $_filter;
    private $_managerInterface;
    private $_collectionFactory;
    private $_frisboFulfillment;
    private $_frisboOrderStatus;
    private $_frisboOrderHelper;
    private $_frisboLogger;

    protected $redirectUrl = 'sales/order/';

    /**
     * @param Context $context
     * @param FrisboFulfillment $frisboClient
     */
    public function __construct(
        Context $context,
        Filter $filter,
        ManagerInterface $managerInterface,
        CollectionFactory $collectionFactory,
        FrisboFulfillment $frisboFulfillment,
        FrisboOrderHelper $frisboOrderHelper,
        FrisboOrderStatusHelper $frisboOrderStatus,
        Logger $frisboLogger
    ) {
        $this->_collectionFactory = $collectionFactory;
        $this->_managerInterface = $managerInterface;
        $this->_frisboFulfillment = $frisboFulfillment;
        $this->_filter = $filter;
        $this->_frisboOrderStatus = $frisboOrderStatus;
        $this->_frisboLogger = $frisboLogger;
        $this->_frisboOrderHelper = $frisboOrderHelper;
        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $collection = $this->_filter->getCollection($this->_collectionFactory->create());
            $this->massAction($collection);
        } catch (\Exception $e) {
            $this->_managerInterface->addErrorMessage($e->getMessage());
        }

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath($this->redirectUrl);
    }

    public function massAction(Collection $collection)
    {
        $orders = $collection->toArray(['entity_id'])['items'];
        $orderIds = array_column($orders, 'entity_id');
        $failed = $this->_frisboFulfillment->syncOrders(...$orderIds);
        if (count($failed) == 0) {
            $this->_managerInterface->addSuccessMessage("Orders synced with Frisbo.");
            return;
        }
        foreach ($failed as $failedOrder) {
          $this->_managerInterface->addErrorMessage("Order $failedOrder->order_reference failed because $failedOrder->reason_status");
        }
    }
}
