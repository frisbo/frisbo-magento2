<?php

namespace Frisbo\MagentoConnector\Controller\Adminhtml\Sync;

use Frisbo\MagentoConnector\Helper\FrisboFulfillment;
use Frisbo\MagentoConnector\Helper\FrisboStockHelper;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Ui\Component\MassAction\Filter;

/**
 * Class Stock
 */
class Stock extends Action
{

    private $_filter;
    private $_managerInterface;
    private $_collectionFactory;
    private $_frisboFulfillment;
    private $_frisboStockHelper;

    protected $redirectUrl = 'catalog/product/';

    /**
     * @param Context $context
     * @param FrisboFulfillment $frisboClient
     */
    public function __construct(
        Context $context,
        Filter $filter,
        ManagerInterface $managerInterface,
        CollectionFactory $collectionFactory,
        FrisboFulfillment $frisboFulfillment,
        FrisboStockHelper $frisboStockHelper
    ) {
        $this->_collectionFactory = $collectionFactory;
        $this->_managerInterface = $managerInterface;
        $this->_frisboFulfillment = $frisboFulfillment;
        $this->_frisboStockHelper = $frisboStockHelper;
        $this->_filter = $filter;
        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $collection = $this->_filter->getCollection($this->_collectionFactory->create());
            $this->massAction($collection);
        } catch (\Exception $e) {
            $this->_managerInterface->addErrorMessage($e->getMessage());
        }

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath($this->redirectUrl);
    }

    public function massAction(Collection $collection)
    {
        $productIds = array_column(array_values($collection->toArray(['entity_id'])), 'entity_id');

        $websites = $this->_frisboStockHelper->getEnabledMagentoWebsites();
        foreach($websites as $website) {
          $this->_frisboFulfillment->syncStocksByProductIds($website, ...$productIds);
          $this->_managerInterface->addSuccessMessage("Stocks synced with Frisbo for website ". $website->getId());
        }

        return;
    }
}
