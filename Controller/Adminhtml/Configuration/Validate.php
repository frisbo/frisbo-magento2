<?php

namespace Frisbo\MagentoConnector\Controller\Adminhtml\Configuration;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Frisbo\MagentoConnector\Helper\FrisboClient;

class Validate extends Action
{

    protected $resultJsonFactory;

    /**
     * @var Data
     */
    protected $frisboClient;

    /**
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param FrisboClient $frisboClient
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        FrisboClient $frisboClient
    )
    {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->frisboClient = $frisboClient;
        parent::__construct($context);
    }

    /**
     * Collect relations data
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $username = $this->getRequest()->getParam('username', null);
        $password = $this->getRequest()->getParam('password', null);
        try {
            $jsonResult = $this->frisboClient->login($username, $password);
            if ($jsonResult == null) {
                return $this->errorResult("Credentials not valid.");
            }
            $result = $this->resultJsonFactory->create();
            $result->setData(json_decode($jsonResult, true));
            return $result;
        } catch (\Exception $e) {
            $this->_objectManager->get('Psr\Log\LoggerInterface')->error($e);
        }

        return $this->errorResult("Credentials not valid.");
    }

    private function errorResult(string $message = ""): \Magento\Framework\Controller\Result\Json
    {
        $result = $this->resultJsonFactory->create();
        $result->setData(['success' => false, "message" => $message]);
        $result->setStatusHeader('403');
        return $result;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Frisbo_MagentoConnector::configuration');
    }
}
