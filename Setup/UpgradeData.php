<?php

namespace Frisbo\MagentoConnector\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var EavSetupFactory
     */
    protected $setupFactory;

    /**
     * InstallData constructor.
     * @param EavSetupFactory $setupFactory
     */
    public function __construct(
        EavSetupFactory $setupFactory
    ) {
        $this->setupFactory = $setupFactory;
    }

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.3.0', '<')) {
            $productAttributesToAdd = [
                'frisbo_product_reservations' => [
                    'group' => 'Frisbo eFulfillment',
                    'type' => 'int',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Reservations',
                    'input' => 'text',
                    'class' => '',
                    'source' => '',
                    'global' => ScopedAttributeInterface::SCOPE_WEBSITE,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => 0,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique' => false,
                    'apply_to' => 'simple',
                    'is_used_in_grid' => false,
                ]
            ];
            /** @var EavSetup $setup */
            $productSetup = $this->setupFactory->create(['setup' => $setup]);
            foreach ($productAttributesToAdd as $name => $attribute) {
                $productSetup->removeAttribute(Product::ENTITY, $name);
                $productSetup->addAttribute(Product::ENTITY, $name, $attribute);
            }
        }
        if (version_compare($context->getVersion(), '1.4.1', '<')) {
            /** @var EavSetup $setup */
            $productSetup = $this->setupFactory->create(['setup' => $setup]);
            $productSetup->updateAttribute('frisbo_product_id', 'global', \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE);
            $productSetup->updateAttribute('frisbo_product_ean', 'global', \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE);
            $productSetup->updateAttribute('frisbo_product_has_serial_number', 'global', \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL);
            $productSetup->updateAttribute('frisbo_product_reservations', 'global', \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE);
        }
    }
}
