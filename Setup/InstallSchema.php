<?php

namespace Frisbo\MagentoConnector\Setup;

use Frisbo\MagentoConnector\Api\Data\FrisboOrderStatusInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $this->createFrisboOrdersTable($setup);

        $setup->endSetup();
    }

    /**
     *
     * @param SchemaSetupInterface $setup
     *
     * @return void
     */
    public function createFrisboOrdersTable(SchemaSetupInterface $setup)
    {
        $table = $setup->getConnection()->newTable($setup->getTable('frisbo_order_statuses'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true,],
                'Order Queue Entry ID'
            )->addColumn(
                'magento_order_id',
                Table::TYPE_INTEGER,
                10,
                ['nullable' => false, 'unsigned' => true,],
                'Magento Order ID'
            )->addColumn(
                'order_id',
                Table::TYPE_BIGINT,
                null,
                ['nullable' => false, 'unsigned' => true,],
                'Frisbo Order ID'
            )->addColumn(
                'status',
                Table::TYPE_TEXT,
                50,
                ['nullable' => false, 'unsigned' => true,],
                'Frisbo Order Status'
            )->addColumn(
                'reason_status',
                Table::TYPE_TEXT,
                50,
                ['nullable' => false,],
                'Reason status'
            )->addColumn(
                'shipping_tracking_number',
                Table::TYPE_TEXT,
                50,
                ['nullable' => false,],
                'Shipping tracking number'
            )->addColumn(
                'serial_number',
                Table::TYPE_TEXT,
                1024,
                ['nullable' => false,],
                'Serial numbers'
            )->addColumn(
                'awb_url',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false,],
                'Awb url'
            )->addColumn(
                'order_hash',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Frisbo Order Hash'
            )->addColumn(
                'frisbo_created_at',
                Table::TYPE_DATETIME,
                null,
                ['nullable' => false],
                'Frisbo Created At'
            )->addColumn(
                'updated_at',
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                'Updated At'
            );

        $table->addIndex(
            $setup->getIdxName('frisbo_order_statuses_order_id', ['order_id']),
            ['order_id'],
            ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
        );

        $table->addForeignKey(
            $setup->getFkName(
                $setup->getTable('frisbo_order_statuses'),
                FrisboOrderStatusInterface::MAGENTO_ORDER_ID,
                $setup->getTable('sales_order'),
                'entity_id'
            ),
            FrisboOrderStatusInterface::MAGENTO_ORDER_ID,
            $setup->getTable('sales_order'),
            'entity_id',
            Table::ACTION_CASCADE
        );

        $setup->getConnection()->createTable($table);
    }
}
