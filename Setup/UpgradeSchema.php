<?php

namespace Frisbo\MagentoConnector\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
	public function upgrade( SchemaSetupInterface $setup, ModuleContextInterface $context ) {
		$installer = $setup;

		$installer->startSetup();

        if (version_compare($context->getVersion(), '1.8.5', '<')) {
            if ($installer->getConnection()->tableColumnExists('frisbo_order_statuses', 'created_at')){
                $definition = [
                    'type' => Table::TYPE_DATETIME,
                    'nullable' => false,
                    'comment' => 'Frisbo Created At'
                ];
                $installer->getConnection()->changeColumn(
                    $setup->getTable('frisbo_order_statuses'),
                    'created_at',
                    'frisbo_created_at',
                    $definition
                );
            }
        }

		$installer->endSetup();
	}
}
