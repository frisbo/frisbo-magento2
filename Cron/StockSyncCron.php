<?php

namespace Frisbo\MagentoConnector\Cron;

use Frisbo\MagentoConnector\ApiModels\Filters\Filter;
use Frisbo\MagentoConnector\Helper\FrisboFulfillment;
use Frisbo\MagentoConnector\Helper\FrisboStockHelper;

class StockSyncCron
{
    private $_frisboFulfillment;
    private $_frisboStockHelper;

    public function __construct(
        FrisboFulfillment $frisboFulfillment,
        FrisboStockHelper $frisboStockHelper
    ) {
        $this->_frisboFulfillment = $frisboFulfillment;
        $this->_frisboStockHelper = $frisboStockHelper;
    }

    /**
     * Gets 20 of the first updated orders that aren't in terminal status and syncs them from Frisbo
     *
     * @return void
     */
    public function execute()
    {
        $filter = new Filter('updated_at', date_create('-30 minutes')->format('Y-m-dTH:i:s'), 'greater_than');
        $magentoWebsiteIds = $this->_frisboStockHelper->getMagentoWebsiteIds();
        foreach ($magentoWebsiteIds as $websiteId) {
            $this->_frisboFulfillment->syncStocksByFilterForWebsite($websiteId, $filter);
        }
        return $this;
    }
}
