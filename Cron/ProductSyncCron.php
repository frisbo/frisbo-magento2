<?php

namespace Frisbo\MagentoConnector\Cron;

use Frisbo\MagentoConnector\Helper\FrisboFulfillment;
use Frisbo\MagentoConnector\Helper\FrisboProductHelper;
use Frisbo\MagentoConnector\Helper\FrisboStockHelper;

class ProductSyncCron
{
    private $_frisboFulfillment;
    private $_frisboProductHelper;
    private $_frisboStockHelper;

    public function __construct(
        FrisboFulfillment $frisboFulfillment,
        FrisboProductHelper $frisboProductHelper,
        FrisboStockHelper $frisboStockHelper
    ) {
        $this->_frisboFulfillment = $frisboFulfillment;
        $this->_frisboProductHelper = $frisboProductHelper;
        $this->_frisboStockHelper = $frisboStockHelper;
    }

    /**
     * Gets 20 of the unsynced products and sends them to Frisbo
     *
     * @return void
     */
    public function execute()
    {
        $magentoWebsites = $this->_frisboStockHelper->getEnabledMagentoWebsites();

        $alreadySyncedStores = [];
        foreach ($magentoWebsites as $website) {
          $storeIds = $website->getStoreIds();
          foreach($storeIds as $storeId) {
            if(isset($alreadySyncedStores[$storeId])) {
              $this->_frisboFulfillment->syncProducts($website, ...$alreadySyncedStores[$storeId]);
              continue;
            }
            $magentoProductsIds = $this->_frisboProductHelper->getUnsyncedMagentoProductIds($storeId, 10);
            $alreadySyncedStores[$storeId] = $magentoProductsIds;

            $this->_frisboFulfillment->syncProducts($website, ...$magentoProductsIds);
          }
        }
        return $this;
    }
}
