<?php

namespace Frisbo\MagentoConnector\Cron;

use Frisbo\MagentoConnector\Helper\FrisboFulfillment;
use Frisbo\MagentoConnector\Helper\FrisboOrderStatusHelper;


class OrderSyncCron
{
    private $_frisboFulfillment;
    private $_frisboOrderStatus;

    public function __construct(
        FrisboFulfillment $frisboFulfillment,
        FrisboOrderStatusHelper $frisboOrderStatus
    ) {
        $this->_frisboFulfillment = $frisboFulfillment;
        $this->_frisboOrderStatus = $frisboOrderStatus;
    }

    /**
     * Gets 20 of the first updated orders that aren't in terminal status and syncs them from Frisbo
     *
     * @return void
     */
    public function execute()
    {
        $magentoOrderIds = $this->_frisboOrderStatus->getNextMagentoOrderIdsToSync();
        $this->_frisboFulfillment->syncOrders(...$magentoOrderIds);
        return $this;
    }
}
