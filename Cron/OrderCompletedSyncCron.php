<?php

namespace Frisbo\MagentoConnector\Cron;

use Frisbo\MagentoConnector\Helper\FrisboFulfillment;
use Frisbo\MagentoConnector\Helper\FrisboOrderHelper;
use Frisbo\MagentoConnector\Helper\FrisboOrderStatusHelper;
use Frisbo\MagentoConnector\Logger\Logger;

class OrderCompletedSyncCron
{
    private $_frisboFulfillment;
    private $_frisboOrderStatus;
    private $_frisboOrderHelper;
    protected $_logger;

    public function __construct(
        FrisboFulfillment $frisboFulfillment,
        FrisboOrderStatusHelper $frisboOrderStatus,
        FrisboOrderHelper $frisboOrderHelper,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->_frisboFulfillment = $frisboFulfillment;
        $this->_frisboOrderStatus = $frisboOrderStatus;
        $this->_frisboOrderHelper = $frisboOrderHelper;
        $this->_logger = $logger;
    }

    /**
     * Get orders that are already in final status (completed) from the last 31 days and sync them again
     *
     * @return void
     */
    public function execute()
    {
        $magentoOrderIds = $this->_frisboOrderStatus->getCompletedMagentoOrderIdsToSync();

        $validatedMagentoOrderIds = [];
        foreach ($magentoOrderIds as $magentoOrderId) {
            if ($this->_frisboOrderHelper->allowSyncForThisCompletedOrder($magentoOrderId)) {
                $validatedMagentoOrderIds[] = $magentoOrderId;
            }
        }
        
        if (empty($validatedMagentoOrderIds)) {
            return;
        }

        $this->_frisboFulfillment->syncOrders(...$validatedMagentoOrderIds);
        return $this;
    }
}
