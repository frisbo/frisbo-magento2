<?php

namespace Frisbo\MagentoConnector\Cron;

use Frisbo\MagentoConnector\Helper\FrisboFulfillment;
use Frisbo\MagentoConnector\Helper\FrisboOrderStatusHelper;

class OrderReturnedSyncCron
{
    private $_frisboFulfillment;
    private $_frisboOrderStatus;

    public function __construct(
        FrisboFulfillment $frisboFulfillment,
        FrisboOrderStatusHelper $frisboOrderStatus
    ) {
        $this->_frisboFulfillment = $frisboFulfillment;
        $this->_frisboOrderStatus = $frisboOrderStatus;
    }

    /**
     * Get orders that are in returned status and sync them in magento
     *
     * @return void
     */
    public function execute()
    {
        $magentoOrderIds = $this->_frisboOrderStatus->getCompletedMagentoOrderIdsToSync();
        $this->_frisboFulfillment->syncOrders(...$magentoOrderIds);
        return $this;
    }
}
