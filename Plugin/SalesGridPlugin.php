<?php

namespace Frisbo\MagentoConnector\Plugin;

use Magento\Sales\Model\ResourceModel\Order\Grid\Collection;
use Magento\Framework\View\Element\UiComponent\DataProvider\Reporting;
use Magento\Framework\Api\Search\SearchCriteriaInterface;
use Frisbo\MagentoConnector\Model\SortOrderWithoutValidation;

class SalesGridPlugin
{
    public static $table = 'sales_order_grid';
    public static $leftJoinTable = 'frisbo_order_statuses';

    public function beforeLoad(Collection $subject)
    {
        if (!$subject->isLoaded()) {
            $subject->getSelect()->joinLeft(
                ['fris' => self::$leftJoinTable],
                'fris.magento_order_id = main_table.entity_id',
                [
                    'frisbo_order_id' => 'fris.order_id',
                    'frisbo_status' => 'fris.status',
                    'frisbo_reason_status' => 'fris.reason_status',
                    'frisbo_serial_number' => 'fris.serial_number',
                ]
            );
        }

        return null;
    }

    public function beforeSearch(Reporting $subject, SearchCriteriaInterface $searchCriteria)
    {
        if ($searchCriteria->getRequestName() == 'sales_order_grid_data_source') {
            $sorting = [];
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                if ($sortOrder->getField()) {
                    $newFieldName = str_replace("frisbo_", "fris.", $sortOrder->getField());
                    $newSortOrder = new SortOrderWithoutValidation();
                    $newSortOrder->setField($newFieldName);
                    $newSortOrder->setDirection($sortOrder->getDirection());
                    $sorting[] = $newSortOrder;
                }
            }
            $searchCriteria->setSortOrders($sorting);

            foreach ($searchCriteria->getFilterGroups() as $filterGroups) {
                foreach ($filterGroups->getFilters() as $filter) {
                    if (strpos($filter->getField(), "frisbo_") !== false) {
                        $newFilterField = $this->nameToTableColumn($filter->getField());
                        $filter->setField($newFilterField);
                    }
                }
            }
        };

        return [$searchCriteria];
    }

    private function nameToTableColumn(string $name)
    {
        return str_replace("frisbo_", "fris.", $name);
    }
}
