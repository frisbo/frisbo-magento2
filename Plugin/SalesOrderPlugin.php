<?php

namespace Frisbo\MagentoConnector\Plugin;

use Exception;
use Frisbo\MagentoConnector\Helper\FrisboFulfillment;
use Frisbo\MagentoConnector\Logger\Logger;
use Magento\Sales\Model\ResourceModel\Order;
use Magento\Framework\Message\ManagerInterface;

class SalesOrderPlugin
{

    private $_frisboLogger;
    private $_frisboFulfillment;
    private $_messageManager;

    public function __construct(
        Logger $frisboLogger,
        ManagerInterface $messageManager,
        FrisboFulfillment $frisboFulfillment
    ) {   
        $this->_frisboLogger = $frisboLogger;
        $this->_messageManager = $messageManager; 
        $this->_frisboFulfillment = $frisboFulfillment;
    }

    public function afterSave(Order $subject, $result, $object) 
    {
        // $object is an AbstractModel, I don't type cast it on purpose since I actually need an OrderInterface for magentoOrderChanged
        $oldStatus = $object->getOrigData('status');
        $newStatus = $object->getData('status');
        $incrementId = $object->getData('increment_id');
        try {
            $this->_frisboLogger->info("FrisboSalesOrderPlugin::Status changed on $incrementId from $oldStatus to $newStatus");
            $this->_frisboFulfillment->magentoOrderChanged($object, $oldStatus);
        } catch(Exception $ex) {
            $this->_messageManager->addErrorMessage("We couldn't sync $incrementId to Frisbo: ". $ex->getMessage());
        }
    }
}
