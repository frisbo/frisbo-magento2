<?php

namespace Frisbo\MagentoConnector\Plugin;

use Frisbo\MagentoConnector\ApiModels\Collections\ChannelCollection;
use Magento\Store\Model\Website;
use Magento\Store\Model\WebsiteRepository;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Message\ManagerInterface;

use Frisbo\MagentoConnector\Logger\Logger;
use Frisbo\MagentoConnector\Helper\FrisboFulfillment;
use Frisbo\MagentoConnector\Exception\AccessForbiden;
use Magento\Store\Model\ScopeTypeNormalizer;

class ApiSettingsPlugin
{
    private $_logger;
    private $_scopeConfig;
    private $_website;
    private $_websiteRepository;
    private $_messageManager;
    private $_scopeTypeNormalizer;

    private $_frisboFulfillment;


    public function __construct(
        Logger $logger,
        ScopeConfigInterface $scopeConfig,
        FrisboFulfillment $frisboFulfillment,
        WebsiteRepository $websiteRepository,
        Website $website,
        ManagerInterface $messageManager,
        ScopeTypeNormalizer $scopeTypeNormalizer
    )
    {
        $this->_logger = $logger;
        $this->_scopeConfig = $scopeConfig;
        $this->_website = $website;
        $this->_websiteRepository = $websiteRepository;
        $this->_messageManager = $messageManager;
        $this->_scopeTypeNormalizer = $scopeTypeNormalizer;
        $this->_frisboFulfillment = $frisboFulfillment;
    }

    public function afterSave(\Magento\Config\Model\Config $subject, $result) {
        $this->_frisboFulfillment->clearConfigCache();

        $sectionSaved = $subject->getSection();
        $scopeType = $this->_scopeTypeNormalizer->normalize($subject->getScope(), false);
        $scopeId = $subject->getScopeId();

        $isTokenValid = $this->resetToken();
        if (!$isTokenValid) {
            $this->_messageManager->addErrorMessage(__("Invalid Frisbo username or password. Integration will not work. Please try again."));
        }

        if ($sectionSaved == 'frisbo_api_settings') {
            if ($isTokenValid) {
                $this->_messageManager->addNoticeMessage(__("Remember to choose your account and enable the Frisbo integration in Website Settings."));
            }
            $this->_frisboFulfillment->setApiSettingsValid($isTokenValid);

            if ($scopeType == 'website') {
              $currentOrganizationId = $this->resetOrganizations($scopeId);
              $this->resetChannels($scopeId, $currentOrganizationId);
            } else {
              $currentOrganizationId = $this->resetOrganizations();
              $this->resetChannels(null, $currentOrganizationId);
            }
        }

        if ($sectionSaved == 'frisbo_channel_settings') {
            $scopeType = $this->_scopeTypeNormalizer->normalize($subject->getScope(), false);
            $scopeId = $subject->getScopeId();
            $this->_logger->info('ApiSettingsPlugin::frisbo_channel_settings in ' . $scopeType . ' scope with id '.$scopeId);

            $selectedSourceCode = $this->_frisboFulfillment->getSelectedSourceCode($scopeType, $scopeId);
            if ($selectedSourceCode == 'none') {
                $this->_messageManager->addWarningMessage(__("No stock source mapped. Stock synchronization will not be available."));
            }
        }

        $this->_frisboFulfillment->clearConfigCache();
    }

    private function resetToken(): bool
    {
        try {
            $token = $this->_frisboFulfillment->getClient()->getAccessToken(true);
            if ($token == null) {
                return false;
            }
        } catch(AccessForbiden $ex) {
            $this->_logger->info('ApiSettingsPlugin::invalid credentials');
            return false;
        }

        return true;
    }

    private function resetOrganizations(int $websiteId = null): int
    {
        $organizations = null;
        try {
            $organizations = $this->_frisboFulfillment->getUserOrganizations();
            if (!$organizations->count()) {
                $organizations = null;
            }
        } catch(AccessForbiden $ex) {
            $this->_logger->info("ApiSettingsPlugin::invalid token. Resetting selected organization id.");
            $this->_frisboFulfillment->setSelectedOrganizationId(0);
            return 0;
        }

        if ($organizations == null) {
            $this->_logger->info("ApiSettingsPlugin::User has no organizations. Resetting global selected organization id.");
            $this->_frisboFulfillment->setSelectedOrganizationId(0);
            return 0;
        }

        $selectedOrganizationId = $this->_frisboFulfillment->getSelectedOrganizationId($websiteId);
        foreach ($organizations as $organization) {
            if ($selectedOrganizationId == $organization->organization_id) {
                $this->_logger->info("ApiSettingsPlugin::Organization already selected.");
                return $selectedOrganizationId;
            }
        }

        $firstOrganization = $organizations->get(0);
        $this->_frisboFulfillment->setSelectedOrganizationId($firstOrganization->organization_id);
        $this->_logger->info("ApiSettingsPlugin::Setting organization id to ". $firstOrganization->organization_id);
        return $firstOrganization->organization_id;
    }

    private function resetChannels(int $selectedWebsiteId = null, int $selectedOrganizationId): bool
    {
        // first reset global channel
        if (!$selectedOrganizationId && !$selectedWebsiteId) {
            $this->_logger->info("ApiSettingsPlugin::Organization not set. Resetting global channel mapping.");
            $this->_frisboFulfillment->setSelectedChannelId(0);
        }

        $channels = null;
        try {
            $this->_logger->info("Getting channels for organization $selectedOrganizationId and website $selectedWebsiteId");
            $channels = $this->_frisboFulfillment->getOrganizationChannels($selectedOrganizationId);
            if (!$channels->count()) {
                $channels = null;
            }
        } catch (AccessForbiden $ex) {
            $this->_logger->info("ApiSettingsPlugin::invalid token. Resetting global channel mapping.");
            if(!$selectedWebsiteId) {
              $this->_frisboFulfillment->setSelectedChannelId(0);
            } else {
              $this->_frisboFulfillment->setSelectedChannelId(0, 'website', $selectedWebsiteId);
            }
        }

        $this->_logger->info(json_encode($channels));

        $websites = $this->_websiteRepository->getList();
        foreach ($websites as $website) {
            $websiteId = $website->getId();

            if ($websiteId == 0) {
                continue;
            }

            if($selectedWebsiteId && $selectedWebsiteId != $websiteId) {
              $this->_logger->info("Website is selected but does not match $websiteId");
              continue;
            }

            $this->_logger->info("Website is selected but does not match $websiteId");
            $websiteSelectedChannelId = $this->_frisboFulfillment->getSelectedChannelId('website', $websiteId);
            if ($channels == null) {
                $this->_frisboFulfillment->setSelectedChannelId(0, 'website', $websiteId);
                $this->_logger->info('ApiSettingsPlugin::Resetting website channel id to 0 for website ' . $websiteId);
                continue;
            }

            if (!$websiteSelectedChannelId || !$this->isChannelIdPresentInCollection($websiteSelectedChannelId, $channels)) {
                $firstChannel = $channels->get(0);
                $this->_frisboFulfillment->setSelectedChannelId($firstChannel->id, 'website', $websiteId);
                $this->_logger->info("ApiSettingsPlugin::Setting channel id to " . $firstChannel->id . ' for website '.$websiteId);
            }
        }

        return true;
    }

    private function isChannelIdPresentInCollection(int $channelId,ChannelCollection $channelCollection): bool
    {
        foreach ($channelCollection as $channel) {
            if ($channel->id == $channelId) {
                return true;
            }
        }
        return false;
    }
}
