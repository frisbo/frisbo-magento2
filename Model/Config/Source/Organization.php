<?php

namespace Frisbo\MagentoConnector\Model\Config\Source;

use Frisbo\MagentoConnector\Helper\FrisboFulfillment;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Organization
 * @package Frisbo\MagentoConnector\Model\Config\Source
 */
class Organization implements OptionSourceInterface
{

    private $_frisboFulfillment;

    public function __construct(FrisboFulfillment $frisboFulfillment)
    {
        $this->_frisboFulfillment = $frisboFulfillment;
    }

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        $organizations = $this->_frisboFulfillment->getUserOrganizations();

        $optionArray = [];
        foreach ($organizations as $organization) {
            $optionArray[] = ['label' => $organization->name, 'value' => $organization->organization_id];
        }

        if (empty($optionArray)) {
            $optionArray[] = ['label' => '-- Error getting organizations --', 'value' => 0];
        }

        return $optionArray;
    }
}
