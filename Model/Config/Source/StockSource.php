<?php

namespace Frisbo\MagentoConnector\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\InventoryApi\Api\SourceRepositoryInterface;

/**
 * Class StockSource
 * @package Frisbo\MagentoConnector\Model\Config\Source
 */
class StockSource implements OptionSourceInterface
{
    private $_sourceRepository;

    public function __construct(SourceRepositoryInterface $sourceRepository)
    {
        $this->_sourceRepository = $sourceRepository;
    }

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        $sources = $this->_sourceRepository->getList()->getItems();

        $optionArray = [];
        $optionArray[] = ['label' => '-- Source not set --', 'value' => 'none'];
        foreach ($sources as $source) {
            $optionArray[] = ['label' => $source->getName(), 'value' => $source->getSourceCode()];
        }

        return $optionArray;
    }
}
