<?php

namespace Frisbo\MagentoConnector\Model\Config\Source;

use Frisbo\MagentoConnector\Helper\FrisboFulfillment;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
/**
 * Class Channel
 * @package Frisbo\MagentoConnector\Model\Config\Source
 */
class Channel implements OptionSourceInterface
{

    private $_frisboFulfillment;
    private $_request;
    private $_scopeConfig;

    public function __construct(FrisboFulfillment $frisboFulfillment, RequestInterface $request, ScopeConfigInterface $scopeConfig)
    {
        $this->_frisboFulfillment = $frisboFulfillment;
        $this->_request = $request;
        $this->_scopeConfig = $scopeConfig;
    }

    public function getWebsiteId() {
      return (int) $this->_request->getParam('website');
    }

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        $organizationId = $this->_frisboFulfillment->getSelectedOrganizationId($this->getWebsiteId());
        if (!$organizationId) {
            return [['label' => '--First select an organization--', 'value' => 0]];
        }

        $channels = $this->_frisboFulfillment->getOrganizationChannels($organizationId);
        if (!$channels->count()) {
            $channels = [];
        }

        $optionArray = [];
        foreach ($channels as $channel) {
            $optionArray[] = ['label' => $channel->name, 'value' => $channel->id];
        }

        if (empty($optionArray)) {
            $optionArray[] = ['label' => '-- Error getting channels --', 'value' => 0];
        }

        return $optionArray;
    }
}
