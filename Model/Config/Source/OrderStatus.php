<?php

namespace Frisbo\MagentoConnector\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory;

/**
 * Class OrderStatus
 * @package Frisbo\MagentoConnector\Model\Config\Source
 */
class OrderStatus implements OptionSourceInterface
{

    /**
     * @var CollectionFactory $statusCollectionFactory
     */
    protected $statusCollectionFactory;


    /**
     * Construct
     *
     * @param CollectionFactory $statusCollectionFactory
     */
    public function __construct(
        CollectionFactory $statusCollectionFactory
    ) {
        $this->statusCollectionFactory = $statusCollectionFactory;
    }


    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        $options = $this->statusCollectionFactory->create()->toOptionArray();
        return $options;
    }
}
