<?php

namespace Frisbo\MagentoConnector\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Frisbo\MagentoConnector\Api\FrisboOrderStatusRepositoryInterface;
use Frisbo\MagentoConnector\Model\FrisboOrderStatusSearchResultFactory;
use Frisbo\MagentoConnector\Model\FrisboOrderStatus;
use Frisbo\MagentoConnector\Model\ResourceModel\FrisboOrderStatus as FrisboOrderStatusResourceModel;
use Frisbo\MagentoConnector\Model\FrisboOrderStatusFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Frisbo\MagentoConnector\Model\ResourceModel\FrisboOrderStatus\CollectionFactory;

/**
 * Class FrisboOrderStatusRepository
 */
class FrisboOrderStatusRepository implements FrisboOrderStatusRepositoryInterface
{
    /**
     * @var ResourceModel
     */
    protected $resourceModel;

    /**
     * @var FrisboOrderStatusFactory
     */
    protected $factory;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var FrisboOrderStatusSearchResultInterfaceFactory
     */
    protected $searchResultFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * AwbRepository constructor.
     *
     * @param FrisboOrderStatusResourceModel $resourceModel
     * @param FrisboOrderStatusFactory $factory
     */
    public function __construct(
        FrisboOrderStatusResourceModel $resourceModel,
        CollectionFactory $collectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        FrisboOrderStatusSearchResultFactory $searchResultFactory,
        FrisboOrderStatusFactory $factory
    ) {
        $this->resourceModel = $resourceModel;
        $this->factory       = $factory;
        $this->collectionFactory   = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    public function create(): FrisboOrderStatus 
    {
        return $this->factory->create();
    }

    public function getByMagentoOrderId(int $magentoOrderId): FrisboOrderStatus 
    {
        $object = $this->factory->create();

        $this->resourceModel->load($object, $magentoOrderId, 'magento_order_id');

        if (!$object->getId()) {
            throw new NoSuchEntityException(__('Frisbo order status for "%1" does not exist.', $magentoOrderId));
        }

        return $object;
    }

    /**
     * {@inheritDoc}
     */
    public function save(FrisboOrderStatus $orderModel)
    {
        $this->resourceModel->save($orderModel);
    }

    /**
     * {@inheritDoc}
     */
    public function getById(int $id): FrisboOrderStatus
    {
        $object = $this->factory->create();

        $this->resourceModel->load($object, $id);

        if (!$object->getId()) {
            throw new NoSuchEntityException(__('Frisbo order status for "%1" does not exist.', $id));
        }

        return $object;
    }

    /**
     * {@inheritDoc}
     */
    public function getList(SearchCriteriaInterface $criteria)
    {
        $searchResult = $this->searchResultFactory->create();
        $collection = $this->collectionFactory->create();
        $this->collectionProcessor->process($criteria, $collection);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());

        return $searchResult;
    }

}
