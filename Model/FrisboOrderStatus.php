<?php

namespace Frisbo\MagentoConnector\Model;

use Frisbo\MagentoConnector\Api\Data\FrisboOrderInterface;
use Frisbo\MagentoConnector\Api\Data\FrisboOrderStatusInterface;
use Magento\Framework\Model\AbstractModel;
use Frisbo\MagentoConnector\Model\ResourceModel\FrisboOrderStatus as FrisboOrderStatusResourceModel;

/**
 * Class FrisboOrderStatus
 */
class FrisboOrderStatus extends AbstractModel implements FrisboOrderStatusInterface
{
    /**
     * {@inheritDoc}
     */
    protected function _construct()
    {
        $this->_init(FrisboOrderStatusResourceModel::class);
    }

    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * {@inheritDoc}
     */
    public function setMagentoOrderId(int $id)
    {
        $this->setData(self::MAGENTO_ORDER_ID, $id);
    }

    /**
     * {@inheritDoc}
     */
    public function getMagentoOrderId()
    {
        return $this->getData(self::MAGENTO_ORDER_ID);
    }

    /**
     * {@inheritDoc}
     */
    public function setOrderId(int $id)
    {
        $this->setData(self::ORDER_ID, $id);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrderId()
    {
        return $this->getData(self::ORDER_ID);
    }

    /**
     * {@inheritDoc}
     */
    public function setStatus(string $status)
    {
        $this->setData(self::STATUS, $status);
    }

    /**
     * {@inheritDoc}
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * {@inheritDoc}
     */
    public function setReasonStatus(string $reasonStatus)
    {
        $this->setData(self::REASON_STATUS, $reasonStatus);
    }

    /**
     * {@inheritDoc}
     */
    public function getReasonStatus()
    {
        return $this->getData(self::REASON_STATUS);
    }

    /**
     * {@inheritDoc}
     */
    public function setShippingTrackingNumber(string $trackingNumber)
    {
        $this->setData(self::SHIPPING_TRACKING_NUMBER, $trackingNumber);
    }

    /**
     * {@inheritDoc}
     */
    public function getShippingTrackingNumber()
    {
        return $this->getData(self::SHIPPING_TRACKING_NUMBER);
    }

    /**
     * {@inheritDoc}
     */
    public function setSerialNumber(string $serialNumber)
    {
        $this->setData(self::SERIAL_NUMBER, $serialNumber);
    }

    /**
     * {@inheritDoc}
     */
    public function getSerialNumber()
    {
        return $this->getData(self::SERIAL_NUMBER);
    }

    /**
     * {@inheritDoc}
     */
    public function setAwbUrl(string $url)
    {
        $this->setData(self::AWB_URL, $url);
    }

    /**
     * {@inheritDoc}
     */
    public function getAwbUrl()
    {
        return $this->getData(self::AWB_URL);
    }

    /**
     * {@inheritDoc}
     */
    public function setOrderHash(string $hash)
    {
        $this->setData(self::ORDER_HASH, $hash);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrderHash()
    {
        return $this->getData(self::ORDER_HASH);
    }

    /**
     * {@inheritDoc}
     */
    public function setCreatedAt(string $date)
    {
        $this->setData(self::CREATED_AT, $date);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * {@inheritDoc}
     */
    public function setUpdatedAt(string $date)
    {
        $this->setData(self::UPDATED_AT, $date);
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }
}
