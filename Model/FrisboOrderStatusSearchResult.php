<?php

namespace Frisbo\MagentoConnector\Model;

use Magento\Framework\Api\SearchResults;
use Frisbo\MagentoConnector\Api\FrisboOrderStatusSearchResultInterface;

class FrisboOrderStatusSearchResult extends SearchResults implements FrisboOrderStatusSearchResultInterface
{
}
