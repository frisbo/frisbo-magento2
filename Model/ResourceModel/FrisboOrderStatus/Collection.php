<?php

namespace Frisbo\MagentoConnector\Model\ResourceModel\FrisboOrderStatus;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Frisbo\MagentoConnector\Model\FrisboOrderStatus as FrisboOrderStatusModel;
use Frisbo\MagentoConnector\Model\ResourceModel\FrisboOrderStatus as FrisboOrderStatusResourceModel;

/**
 * Class FrisboOrderCollection
 */
class Collection extends AbstractCollection
{
    protected $_idFieldName = FrisboOrderStatusModel::ID;

    /**
     * {@inheritDoc}
     */
    protected function _construct()
    {
        $this->_init(FrisboOrderStatusModel::class, FrisboOrderStatusResourceModel::class);
    }
}
