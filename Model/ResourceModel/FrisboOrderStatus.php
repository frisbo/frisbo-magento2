<?php

namespace Frisbo\MagentoConnector\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Frisbo\MagentoConnector\Api\Data\FrisboOrderStatusInterface;

/**
 * Class FrisboOrderStatus
 */
class FrisboOrderStatus extends AbstractDb
{
    protected $_idFieldName = FrisboOrderStatusInterface::ID;

    /**
     * {@inheritDoc}
     */
    protected function _construct()
    {
        $this->_init('frisbo_order_statuses', FrisboOrderStatusInterface::ID);
    }
}
