<?php

namespace Frisbo\MagentoConnector\Model;

use Magento\Framework\Api\SortOrder;

class SortOrderWithoutValidation extends SortOrder
{
    /**
     * Set sorting field.
     *
     * @param string $field
     * @throws InputException
     *
     * @return $this
     */
    public function setField($field)
    {
        return $this->setData(SortOrder::FIELD, $field);
    }
}