<?php

namespace Frisbo\MagentoConnector\Exception;

use Exception;

/**
 * Class OrderExists
 * @package Frisbo\MagentoConnector\OrderException
 */
class OrderException extends Exception
{
}
