<?php

namespace Frisbo\MagentoConnector\Exception;

/**
 * Class ProductExists
 * @package Frisbo\MagentoConnector\Exception
 */
class ProductExists extends \Exception
{
}
