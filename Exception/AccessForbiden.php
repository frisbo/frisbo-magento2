<?php

namespace Frisbo\MagentoConnector\Exception;

/**
 * Class AccessForbiden
 * @package Frisbo\MagentoConnector\Exception
 */
class AccessForbiden extends \Exception
{
}
